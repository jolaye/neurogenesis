import numpy as np
import pandas as pd
import json

from theoric.cells import *
from utilities.utils import mean_var_cells


class Params_layers(object):
    """
    Cette classe est celle que l'on va utiliser lors de l'optimisation des paramètres 
    liés au choix des couches lors de la création des neurones. Notamment, nous 
    pouvons à l'aide de cette classe calculer la valeur du critère à optimiser 
    (défini comme étant l'écart entre l'espérance théorique et l'espérance 
    expérimentale au carré pour les DLs et ULs), mais aussi la valeur de son gradient. 
    Cette classe sera appelée lorsque l'on effectuera l'optimisation par descente 
    de gradient  ou par algorithme génétique.

    Attributes
    ------------
    type_sample : str
        Permet de savoir si nous allons l'optimisation avec le jeu de données des
        souris normales (Normal) ou mutantes (Mutant). 
    
    type_division : str
        Permet de choisir le type de loi que l'on va prendre pour le temps de division
        de chacun des types de cellules (gamma, constant etc...)
    
    data_exp: str
        Nom du fichier qui possède les données expérimentales que nous allons 
        utiliser.
    
    params_bornes : list
        Liste qui contient les différentes bornes des paramètres. La liste est en
        fait une liste contenant 2 listes. La première contient les bornes inférieures
        pour les 4 paramètres à optimiser, la seconde les bornes supérieures.
    
    params_name : list
        Nom des différents paramètres que l'on cherche à optimiser.
    """

    def __init__(
        self,
        type_sample: str = "Normal",
        type_division: str = "constant",
        data_exp: str = "data/experimental/data_exp.csv",
        params_bornes: list = [[0.1, 12, 0.05, 0.55], [5, 18, 0.45, 0.95]],
        params_name: list = [
            "Slope inflexion",
            "Location inflexion",
            "Asymp +inf",
            "Asymp -inf",
        ],
    ):

        # Valeurs min/max que peuvent prendre les paramètres
        self.params_min, self.params_max = params_bornes

        # Récupération des données expérimentales
        self.data = pd.read_csv(data_exp)

        # Cette fonction aura besoin de ces deux classes lors des calculs
        self.deep_layers = DeepLayers(type_sample, type_division)
        self.up_layers = UpLayers(type_sample, type_division)

        # Noms des différents paramètres
        self.params_name = params_name

    def compute_val(
        self, params_layers: "np.array", compute_grad: bool = False,
    ) -> (float, "np.array"):
        """
        Cette fonction est la fonction qui permet de calculer la valeur du critère
        d'optimisation, mais aussi le gradient de ce critère si on le souhaite. 
        Le critère est défini comme l'écart au carré entre l'espérance théorique 
        de DL et la moyenne de DL expérimentale + l'écart au carré entre l'espérance 
        de UL et la moyenne de UL expérimentale pour les différents temps auxquels
        on a effectué nos expériences. 

        Parameters
        ------------
        params_layers : "np.array"
            Paramètres pour lesquels nous souhaitons obtenir la valeur du critère.

        compute_grad : bool 
            Permet de choisir si l'on veut ou non renvoyer la valeur du gradient 
            associé à ce critère.
        """

        times, means_DL_exp, vars_DL_exp = mean_var_cells(self.data, "DL")
        times, means_UL_exp, vars_UL_exp = mean_var_cells(self.data, "UL")  # Same times

        value = 0
        coeff_value = (
            1 / 2
        )  # Coefficient devant le critère, présent devant le gradient du critère si on ne le mettait pas

        if compute_grad:
            grad = np.zeros(len(params_layers))

        for k in range(0, len(times)):

            self.deep_layers.update_layers_parameters(params_layers)
            self.up_layers.update_layers_parameters(params_layers)

            ecart_exp_DL = self.deep_layers.esperance(times[k]) - means_DL_exp[k]
            ecart_exp_UL = self.up_layers.esperance(times[k]) - means_UL_exp[k]

            value += coeff_value * (ecart_exp_DL ** 2 + ecart_exp_UL ** 2)

            if (
                compute_grad
            ):  # Pas besoin d'allourdir les calculs si on n'a pas besoin de le calculer
                grad += ecart_exp_DL * self.deep_layers.gradient_esperance(
                    times[k]
                ) + ecart_exp_UL * self.up_layers.gradient_esperance(times[k])

        if compute_grad:
            return value, grad

        else:
            return value
