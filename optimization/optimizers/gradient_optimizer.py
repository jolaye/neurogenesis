import numpy as np
import json
import scipy.integrate as integrate
from alive_progress import alive_bar
import warnings

warnings.filterwarnings("ignore")  # Des warnings vont être affichées à cause
# d'exponentielles qui vont valoir +infini,
# elles ne posent pas problème car de telles  valeurs vont être de
# toute façon rejetée par notre algo.


class Gradient_optimizer(object):
    """
    Cette classe s'agit de la classe qui permet d'effectuer une  optimisation 
    à l'aide d'une descente de gradient projetée, dont le pas est choisi par
    la règle d'Armijo. Cette classe prend en compte la fonction qui gère la 
    descente, ainsi que les fonctions de projection et de choix de pas.

    Examples
    ------------
    func_to_opt = Params_layers()
    optimizer = Gradient_optimizer(func_to_opt)
    params, vals = optimizer.optimize()

    Attributes
    ------------
    func_to_opti: "Class"
        Fonction que nous allons chercher à optimiser.
    """

    def __init__(self, func_to_opti: "Class"):
        self.func_to_opti = func_to_opti
        self.dim_params = len(self.func_to_opti.params_name)

    def optimize(self, param_init: "np.array" = None) -> ("np.array", "np.array"):
        """
        Cette classe permet de lancer l'optimisation par descente de gradient. 
        Soit les paramètres initiaux sont donnés et l'optimisation par descente de 
        gradient se fait en partant de ce paramètre, soit aucun n'est donné et 
        dans ce cas les paramètres initiaux par la méthode multistart.

        Parameters
        ------------
        param_init : "np.array"
            Valeur du paramètre initial. Si rien n'est donné, il sera trouvé par
            la méthode multistart.
        
        Returns
        ------------
        params : "np.array"
            Historique des paramètres à chaque itération.
        
        vals : "np.array"
            Historique des valeurs de la fonction associées aux paramètres à chaque itération.
        """
        if len(param_init) == 0:

            params, vals = self.multi_start_opt()

        else:
            params, vals = self.gradient_descent(
                param_init, stopping_criteria="gradient",
            )

        return params, vals

    def gradient_descent(
        self,
        param_init: "np.array",
        stopping_criteria: str = "iterations",
        max_iterations: int = 200,
        seuil_changement: float = 1e-6,
        seuil_grad: float = 1e-5,
    ) -> ("np.array", "np.array"):
        """
        Cette fonction est celle qui gère la descente de gradient projetée qui 
        part d'un certain paramètre donné (trouvé par la méthode MultiStart).
        À chaque itération, nous allons choisir le pas à l'aide de la règle 
        d'Armijo puis mettre à jour à l'aide de la projection tout ça à l'aide de 
        la valeur du gradient. Cette fonction renvoie l'historique des paramètres/valeur 
        fonction au cours de la descente de gradient.

        Parameters
        ------------
        param_init : "np.array"
            Paramètre initiale dont nous allons partir afin d'effectuer notre descente de
            gradient.
        
        stopping_criteria : str
            Critère d'arrêt pour la descente de gradient. Ce critère d'arrêt pouvant 
            être un nombre maximum d'itérations, l'arrêt de la progression d'un 
            paramètre,les deux ou alors la norme du gradient.
        
        max_iterations : int
            Nombre maximum d'itérations. Aura un effet uniquement si on prend un 
            des critères qui le prend en compte.
    
        seuil_changement : float
            Seuil auquel on considère que les paramètres ne varient plus et donc qui 
            entraîne l'arrêt de la simulation. Aura un effet uniquement si on prend un 
            des critères qui le prend en compte.
        
        seuil_grad : float
            Seuil auquel on considère que la norme du gradient est trop faible 
            et entraîne l'arrêt de la simulation.
        
        Returns
        ------------
        params : "np.array"
            Historique des paramètres à chaque itération.
        
        vals : "np.array"
            Historique des valeurs de la fonction associées aux paramètres à chaque itération.
        """
        # Initialisation
        param = param_init
        continue_optimization = True
        n_iterations = 0

        #  To have the history
        val, grad = self.func_to_opti.compute_val(param, compute_grad=True)

        params = [param_init]
        vals = [val]
        grads = [grad]

        while continue_optimization:

            if stopping_criteria != "iterations":  # On affiche la verbose dans ce cas
                print("-------------")
                print(n_iterations + 1)
                print(param)
                print(grad)
                print(val)

            # Update
            step = self.choose_step(param, grad, val)
            param = self.projector(param - step * grad)
            val, grad = self.func_to_opti.compute_val(param, compute_grad=True)

            # Update history
            params.append(param)
            vals.append(val)
            grads.append(grad)

            # Test stopping criteria
            n_iterations += 1
            if stopping_criteria == "iterations":

                continue_optimization = n_iterations < max_iterations

            if stopping_criteria == "seuil":
                continue_optimization = (
                    np.linalg.norm(params[n_iterations] - params[n_iterations - 1])
                ) > seuil_changement
            if stopping_criteria == "both":
                continue_optimization = (
                    (np.linalg.norm(params[n_iterations] - params[n_iterations - 1]))
                    > seuil_changement
                ) & (n_iterations < max_iterations)

            if stopping_criteria == "gradient":
                continue_optimization = np.linalg.norm(grads[n_iterations]) > seuil_grad

        # Add last_values and to array
        params = np.array(params)
        grads = np.array(grads)
        vals = np.array(vals)
        return params, vals

    def multi_start_opt(
        self,
        n_initial_points: int = 10,
        n_initial_iterations: int = 2,
        n_initial_best_params: int = 3,
        n_second_iterations: int = 3,
    ) -> ("np.array", "np.array"):
        """
        Cette fonction permet de lancer l'optimisation avec la méthode MultiStart 
        qui permet de choisir l'endroit où nous commençons notre descente de gradient.
        Tout d'abord, nous lançons la descente de gradient pour une première vague 
        de points initiaux aléatoirements choisis et nous gardons les meilleurs
        au bout d'un court nombre d'itérations. Nous relançons ensuite ces descentes de 
        gradient avec les paramètres choisis, et nous gardons cette fois la meilleure
        parmi elles au bout encore d'un petit nombre d'itérations. Nous lançons ensuite
        la descente de gradient avec la condition initiale choisi, cette fois jusqu'à 
        ce que les paramètres n'évoluent plus ou qu'un très grand nombre d'itérations 
        a été atteint.

        Parameters
        ------------
        n_initial_point : int 
            Décrit combien de descentes de gradient nous allons lancer lors de la 
            première vague.
        
        n_initial_iterations : int
            Décrit le nombre d'itérations que nous allons effectuer lors de cette 
            première vague. Celui-ci doit être petit.
        
        n_initial_best_params : int 
            Décrit le nombre de paramètres que nous allons garder à la suite de cette première vague.
        
        n_seconds_iterations : int
            Décrit le nombre d'itérations que nous allons prendre pour la deuxième 
            vague de descente de gradient.

        Returns
        ------------
        params : "np.array"
            Historique des paramètres à chaque itération.
        
        vals : "np.array"
            Historique des valeurs de la fonction associées aux paramètres à chaque itération.
        """

        all_vals_first_wave = np.zeros((n_initial_points, n_initial_iterations + 1))
        all_params_first_wave = np.zeros(
            (n_initial_points, n_initial_iterations + 1, self.dim_params)
        )
        print("FIRST WAVE INITIAL CONDITIONS:")
        with alive_bar(n_initial_points) as bar:
            ## On effectue la première vague d'optimisation
            for k in range(0, n_initial_points):
                initial_params = self.generate_random_param()

                params, vals = self.gradient_descent(
                    initial_params,
                    stopping_criteria="iterations",
                    max_iterations=n_initial_iterations,
                )

                all_params_first_wave[k] = params
                all_vals_first_wave[k] = vals

                bar()
        # Choix des meilleurs paramètres durant cette première vague
        index_best_params = np.argsort(all_vals_first_wave[:, -1])[
            :n_initial_best_params
        ]
        all_params_first_wave = all_params_first_wave[index_best_params]
        all_vals_first_wave = all_vals_first_wave[index_best_params]

        print("Best initial conditions at the first wave:")
        print(all_params_first_wave[:, -1])
        print(all_vals_first_wave[:, -1])

        ## On effectue la seconde vague d'optimisation
        all_vals_second_wave = np.zeros(
            (n_initial_best_params, n_second_iterations + 1)
        )
        all_params_second_wave = np.zeros(
            (n_initial_best_params, n_second_iterations + 1, self.dim_params)
        )

        print("SECOND WAVE INITIAL CONDITIONS:")
        with alive_bar(len(all_params_second_wave)) as bar:
            for k in range(0, len(all_params_second_wave)):

                params, vals = self.gradient_descent(
                    all_params_first_wave[k, -1, :],
                    stopping_criteria="iterations",
                    max_iterations=n_second_iterations,
                )
                all_params_second_wave[k] = params
                all_vals_second_wave[k] = vals

                bar()

        # Choix des meilleurs paramètres durant cette seconde vague
        index_best_param = np.argmin(all_vals_second_wave[:, -1])
        params = np.concatenate(
            (
                all_params_first_wave[
                    index_best_param, :-1
                ],  # -1 car les valeurs aux extrémités des deux tableaux sont les mêmes
                all_params_second_wave[index_best_param, :],
            ),
            axis=0,
        )
        vals = np.concatenate(
            (
                all_vals_first_wave[index_best_param, :-1],
                all_vals_second_wave[index_best_param, :],
            ),
            axis=0,
        )
        print("Chosen initial condition:")
        print(params[-1])
        print(vals[-1])

        print("OPTIMIZATION WITH BEST INITIAL CONDITION:")
        ## Optimisation avec cette meilleure condition initiale
        last_params, last_vals = self.gradient_descent(
            params[-1, :], stopping_criteria="both",
        )
        params = np.concatenate((params[:-1], last_params,), axis=0,)
        vals = np.concatenate((vals[:-1], last_vals), axis=0,)

        return params, vals

    def generate_random_param(self) -> "np.array":
        """
        Cette fonction permet de générer aléatoirement les paramètres initiaux. Nous 
        allons nous en servir lors de la méthode multistart afin de choisir le 
        paramètre initial de notre descente de gradient. Cette génération aléatoire
        se fait dans l'intervalle de valeurs associé à notre fonction.

        Returns
        ------------
        initial_params:
            Valeur des paramètres générés aléatoirement.
        """
        initial_params = np.zeros(self.dim_params)

        for k in range(0, self.dim_params):

            initial_params[k] = np.random.uniform(
                low=self.func_to_opti.params_min[k],
                high=self.func_to_opti.params_max[k],
            )

        return initial_params

    def projector(self, param: "np.array") -> float:
        """
        Cette fonction permet pour un tableau de paramètres donné de calculer sa projection 
        sur le pavé sur lequel on effectue notre problème d'optimisation.
        Cette projection se fait assez simplement : si la coordonnée d'un paramètre 
        est hors du pavé alors la coordonée de sa projection sera soit l'extrémité
        supérieure soit l'extrémité inférieure en fonction du fait que la coordonée
        que l'on cherche à projeter est plus grande ou plus petite que les valeurs 
        de l'intervalle.

        Parameters
        ------------
        param : "np.array"
            Paramètre dont on veut obtenir la projection.
        
        Returns
        ------------
        proj_params : float
            Valeur de la projection de nos paramètres.
        """
        proj_params = np.zeros(self.dim_params)

        for k in range(0, self.dim_params):
            proj_params[k] = min(
                max(self.func_to_opti.params_min[k], param[k]),
                self.func_to_opti.params_max[k],
            )

        return proj_params

    def choose_step(self, param: "np.array", grad: "np.array", val: float) -> float:
        """
        Cette fonction permet de choisir le pas pour chaque itération à l'aide de 
        la règle d'Armijo. Cette règle consiste à choisir le premier pas pour lequel
        nous avons une baisse (suffisante) du critère d'optimisation.

        Parameters
        ------------
        param : "np.array"
            Paramètre avant mise à jour lors de l'itération.
        
        grad : "np.array"
            Valeur du gradient pour le paramètre avant mise à jour.
        
        val : float
            Valeur du critère d'optimisation avant mise à jour lors de l'itération.
        """
        # Paramètres initiaux classiques
        min_step = 1e-15
        step = 0.1
        geom_multip = 0.1
        coeff_grad = 0.01
        loop_continue = True

        new_param = self.projector(param - step * grad)
        while loop_continue:

            step *= geom_multip

            val_test = self.func_to_opti.compute_val(new_param)
            loop_continue = (
                val_test > val + coeff_grad * np.dot(grad, new_param - param)
            ) & (step > min_step)

            # Change step for the possible next loop
            param = self.projector(param - step * grad)

        return step

