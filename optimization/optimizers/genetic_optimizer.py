import numpy as np
import matplotlib.pyplot as plt
import json


class Genetic_optimizer(object):
    """
    Cette classe permet d'effectuer une optimisation d'une fonction à l'aide
    d'un algorithme génétique. L'algorithme géntique consiste en le fait d'utiliser
    une population d'une certaine taille, qui possède des "chromosomes" décrivant 
    chacun de nos paramètres, et dont chaque individu possède un score de fitness
    lié à la fonction que nous optimisons (plus les paramètres liés aux chromosomes
    des individus donnent une valeur de la fonction basse, plus le score de fitness
    est élevé). À chaque génération, les individus les mieux fittés survivent, 
    et les autres soient mutent, soient sont remplacés par des nouveaux-nés. L'intérết 
    des mutations est que l'on va éviter de tomber dans des minimums locaux. 

    Examples
    ------------
    func_to_opt = Params_layers()
    optimizer = Genetic_optimizer(func_to_opt)
    params, vals = optimizer.optimize()

    Attributes
    ------------
    func_to_opti: "Class"
        Fonction que nous allons chercher à optimiser.

    power_len_params : int 
        Comme nous utilisons une représentation des chromosomes en binaires (0 ou 1),
        pour chaque paramètre nous chercherons sur une grille qui sera une puissance
        de 2. Ce paramètre s'agit de la puissance que nous allons utiliser. Par 
        exemple, si ce paramètre vaut 10, nous chercherons dans une grille de 
        2**10 = 1024 valeurs chaque paramètre.
    """

    def __init__(
        self, func_to_opti: "Class", power_len_params: int = 14,
    ):
        # Paramètres importants
        self.func_to_opti = func_to_opti
        self.n_params = len(self.func_to_opti.params_name)
        self.power_len_params = power_len_params

        # Chromosomes paramétrés avec des 0 ou des 1
        self.low_bit = 0
        self.n_bits = 2

    def initialize_pop(self, size_pop: float) -> "np.array":
        """
        Cette fonction permet d'initialiser une population au début de l'optimisation.
        Si on possède N paramètres et que pour chacun on regarde dans une grille de 
        taille 2**D  la valeur optimale, nous aurons des chromosomes de taille 
        NxD de par leur représentation binaire. Nous générons donc de manière 
        aléatoire une population de taille M, dont chaque individu est caractérisé
        par un chromosome de taille NxD dont chaque "nucléotide" vaut soit 0 soit 1.

        Parameters
        ------------
        size_pop : float
            Nombre d'individus dans la population que nous allons créer.

        Return
        ------------
        population : "np.array"
            Population générée par la fonction.
        """
        # Creation population de manière aléatoire
        size_chromosomes = self.power_len_params * self.n_params
        population = np.random.randint(
            self.low_bit, self.n_bits, size=(size_pop, size_chromosomes)
        )

        return population

    def decode_chrom(self, chromosome: "np.array") -> "np.array":
        """
        Cette fonction permet d'obtenir l'information contenue dans chaque chromosome,
        c'est-à-dire pour un chromosome donné, obtenir les paramètres qui lui sont 
        associés.

        Pour rappel, le chromosome est de taille NxD où N est le 
        nombre de paramètre, et D est tel que la taille de la grille de chaque 
        paramètre est 2**D. Sur chaque plage kD:(k+1)D du chromosome, nous avons 
        la valeur en binaire de l'indice du paramètre k, dans le tableau 
        linspace(min_params[k], max_params[k], 2**D). Nous allons obtenir ici 
        chacune de ces valeurs.

        Parameters
        ------------
        chromosome : "np.array"
            Chromosome dont on souhaite obtenir la valeur de ces paramètres associés.

        Return
        ------------
        params_chrom : "np.array"
            Valeur de paramètre associé au chromosome que nous avons décodé.
        """
        ### Décodage de l'information que possède le chromosome
        params_chrom = np.zeros(
            self.n_params
        )  # Possède toutes les informations des chromosomes
        for k in range(0, self.n_params):
            indiv_param_chrom = chromosome[
                k * self.power_len_params : (k + 1) * self.power_len_params
            ]  # Information du paramètre k contenue dans le chromosome
            indiv_param_chrom = sum(
                [
                    (2 ** l) * indiv_param_chrom[l]
                    for l in range(0, self.power_len_params)
                ]
            )  # Conversion du chromosome (sous forme binaire) en sa valeur en décimale

            params_chrom[k] = np.linspace(
                self.func_to_opti.params_min[k],
                self.func_to_opti.params_max[k],
                self.n_bits ** self.power_len_params,
            )[
                indiv_param_chrom
            ]  # Valeur associée dans le tableau des paramètres linspace(min_param, max_param, n_bits ** self.power_len_params)

        return params_chrom

    def function_for_chrom(self, chromosome: "np.array") -> float:
        """
        Cette fonction permet d'obtenir la valeur du critère d'optimisation pour
        un chromosome donné. Dans un premier temps, le chromosome est décodé en 
        paramètres, et ensuite on calcule la valeur du critère.

        Parameters
        ------------
        chromosome : "np.array"
            Chromosome que l'on va décoder afin d'obtenir la valeur du critère 
            d'optimisation.
        """
        params_chrom = self.decode_chrom(chromosome)

        return self.func_to_opti.compute_val(params_chrom)

    def optimize(
        self,
        size_pop: int = 50,
        max_gen: int = 100,
        n_elites: int = 4,
        n_new_born: float = 20,
        taux_mutation: float = 0.5,
        verbose: bool = True,
    ) -> ("np.array", "np.array"):
        """ 
        Cette fonction permet d'effectuer l'optimisation par algorithme génétique. 
        D'abord on initialise une population de manière aléatoire, ensuite on 
        laisse évoluer cette population un certain nombre de génération, dont les 
        individus vont muter/ se reproduire etc... À chaque individu va être associé
        un score de fitness, qui sera plus élevé si les paramètres associés à son 
        chromosome minimise bien la valeur de la fonction. À la fin, l'algorithme 
        renvoie le paramètre associé au score de fitness le plus élevé (ou de manière
        équivalente, l'individu qui minimise le mieux la fonction vu que le critère
        de fitness n'est pas explicitement invoqué dans la fonction).

        Parameters
        ------------
        size_pop : int 
            Taille de la population qui va évoluer durant notre optimisation.

        max_gen : int 
            Nombre de générations sur lesquelles nous allons laisser évoluer notre
            population.
        
        n_elites : int
            À chaque génération, un certain nombre d'invidu survit automatiquement.
            On les appelle "élites" et ils sont caractérisés par le fait qu'ils ont
            le meilleur score de fitness. Ce paramètre décrit combien d'élites 
            survit lors de chaque génération.
        
        n_new_born : float 
            Nombre d'individus qui vont être remplacés par des nouveaux-nés.
        
        taux_mutation : float 
            Les individus qui ne sont ni des élites, ni remplacés par des nouveaux-nés
            vont muter. Ce paramètre permet de décrire le taux auxquels chacun
            d'eux vont muter. Les chromosomes étant de tailles N*D, le nombre
            de mutations à chaque génération sera taux_mutation*N*D.

        verbose : bool
            Si ce paramètre est sur vrai, alors il sera affiché à chaque itération
            le score de chaque individu ainsi que le meilleur paramètre.
        
        Return
        -----------
        best_params : "np.array"
            Tableau qui donnera la valeur du meilleur paramètre à chaque itération.

        best_params : "np.array"
            Tableau qui donnera la valeur de la fonction pour le  meilleur paramètre 
            à chaque itération.
        """
        # Initialisation population
        self.population = self.initialize_pop(size_pop)
        values = np.zeros(size_pop)

        # Tableaux contenant les meilleurs paramètres à chaque optimisation
        best_params = []
        best_vals = []

        for gen in range(0, max_gen):

            # Calcul des valeurs de fitness
            for indiv in range(0, size_pop):

                values[indiv] = self.function_for_chrom(self.population[indiv])

            # On met dans l'ordre afin de ne garder que les meilleurs
            self.population = self.population[np.argsort(values)]
            values = np.sort(values)
            best_params.append(self.decode_chrom(self.population[np.argmin(values)]))
            best_vals.append(np.min(values))
            if verbose:
                print("---------------------------------------")
                print("Génération {}:".format(gen))
                print("All values:")
                print(values)
                print("Best params:")
                print(best_params[-1])
                print(best_vals[-1])

            # Nouveaux-nés par crossover
            probability_reproduce = (1 / values) / (
                np.sum(1 / values)
            )  # Les mieux fittés (ceux qui minimisent la fonction) ont plus de chance de se reproduire

            new_pop = np.copy(self.population)
            np.random.shuffle(new_pop[n_elites:])

            for indiv in range(n_elites, n_elites + n_new_born):

                # Parents au hasard, ceux qui fittent le mieux ont plus de chance de se reproduire
                index_parent_1 = indiv
                index_parent_2 = indiv

                while index_parent_1 == indiv or index_parent_2 == indiv:
                    index_parent_1 = np.random.choice(
                        np.arange(size_pop), p=probability_reproduce
                    )
                    index_parent_2 = np.random.choice(
                        np.arange(size_pop), p=probability_reproduce
                    )

                # Nouveau-né
                new_pop[indiv] = self.crossover(
                    self.population[index_parent_1], self.population[index_parent_2]
                )

            # Mutations
            for indiv in range(n_elites + n_new_born, size_pop):

                n_mutation = int(
                    taux_mutation * (self.power_len_params) * self.n_params
                )

                for mutation in range(0, n_mutation):
                    new_pop[indiv] = self.mutate(new_pop[indiv])

            # Nouvelle population
            self.population = new_pop

        for indiv in range(0, size_pop):
            values[indiv] = self.function_for_chrom(self.population[indiv])

        best_params = np.array(best_params)
        return best_params, best_vals

    def crossover(self, parent_1: "np.array", parent_2: "np.array") -> "np.array":
        """
        Cette fonction permet de générer un nouveau-né, par reproduction de deux 
        parents donnés. Le nouveau-né est généré de manière à ce que pour chaque 
        paramètre, le nouveau-né hérite de celuis de l'un de ces deux parents 
        avec probabilité égale.
        
        Parameters
        ------------
        parent_1 : "np.array"
            Chromosome du parent 1.
        
        parent_1 : "np.array"
            Chromosome du parent 2.

        Return
        --------
        new_chrom : "np.array"
            Chromosome du nouveau-né.
        """
        new_chrom = np.zeros(
            self.n_params * (self.power_len_params)
        )  # Nouveau chromosome, héritera des paramètres soit du père soit de la mère
        for k in range(0, self.n_params):

            herits_from_parent_1 = int(np.round(np.random.uniform()))

            if herits_from_parent_1:

                new_chrom[
                    k * self.power_len_params : (k + 1) * self.power_len_params
                ] = parent_1[
                    k * self.power_len_params : (k + 1) * self.power_len_params
                ]  # Information du paramètre k contenue dans le chromosome

            else:
                new_chrom[
                    k * self.power_len_params : (k + 1) * self.power_len_params
                ] = parent_2[
                    k * self.power_len_params : (k + 1) * self.power_len_params
                ]

        return new_chrom

    def mutate(self, individu: "np.array") -> "np.array":
        """
        Cette fonction permet de faire muter le chromosome d'un individu. La mutation
        se fait en prenant aléatoirement un des nucléotides du chromosomes et en 
        changeant sa valeur (0 -> 1 et 1 -> 0). 

        Parameters
        ------------
        individu : "np.array"
            Chromosome auquel on souhaite appliquer la mutation.
        
        Return
        ------------
        individu : "np.array"
            Retourne l'individu qui aura subi la mutation.
        """
        index_mutation = np.random.randint(0, len(individu))
        individu[index_mutation] = 1 - individu[index_mutation]  # Flip
        return individu

