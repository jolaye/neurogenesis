import numpy as np
import pandas as pd
import json
from theoric.measures import *
from utilities.functions.sigmoide import *
from utilities.utils import mean_var_cells


class UpLayers(object):
    """
    Cette classe contient toutes les fonctions qui permettent de calculer
    l'espérance ainsi que la variance du nombre de UL en fonction du temps, en
    se servant des différentes mesures possibles. Elle possède également deux fonctions
    qui permettent de calculer le gradient de l'espérance et de la variance du nombre
    de cellules au cours du temps afin de pouvoir effectuer la descente de gradient.

    Attributes
    ------------
    type_sample : str 
        Permet de choisir si nous allons plutôt prendre les paramètres pour la souris
        mutant ou pour la souris normale.
    
    type_division : str
        Permet de choisir le type de loi que l'on va prendre pour le temps de division
        de chacun des types de cellules (gamma, constant etc...)
    """

    def __init__(
        self, type_sample: str = "Normal", type_division: str = "constant",
    ):

        # Initialisation de certains paramètres liés à la simulation
        self.initialize_simu_parameters(type_sample, type_division)
        self.initialize_type_division()

        # Indice des différents paramètres
        self.index_slope = 0
        self.index_location = 1
        self.index_plus_inf = 2
        self.index_moins_inf = 3

    def choose_layer(self, time_event: float = 0) -> "function":
        """
        Cette fonction renvoie la fonction qui permet de choisir si les neurones 
        vont appartenir aux couches profondes ou au couches superficielles. Il existe 
        un paramètre de shiftage car on va devoir souvent utiliser une fonction 
        shiftée lors de l'intégration par les mesures.

        Parameters
        ------------
        time_event : float
            Paramètre de shiftage. Ce shiftage correspond en fait au moment où
            l'évènement se produit.
        """

        def choose_layer_shift(
            time_division: float, time_second_division: float = 0
        ) -> float:
            """
            Cette fonction est la fonction qui permet de choisir si les neurones 
            vont appartenir aux couches profondes ou au couches superficielles. Il existe 
            deux paramètre de shiftage correspondant aux instants de chacunes des divisions en 
            IPP/IPN car en fonction de quand ils ont lieu, la valeur de choose_layer 
            ne sera pas la même.

            Parameters
            ------------
            time_division : float
                Premier paramètre de shiftage. Ce shiftage correspond en fait au moment où
                dans le cas où elle se produit au moment de la division d'un IPP/IPN.

            time_division : float
                Premier paramètre de shiftage. Ce shiftage correspond en fait au moment où
                dans le cas où elle se produit au moment de la division d'un IPN qui 
                était précédemment un IPP.
            """
            return 1 - sigmoide(
                time_event + time_division + time_second_division,
                self.simu_parameters["deep_or_up"]["slope_function_rate"],
                self.simu_parameters["deep_or_up"]["location_function_rate"],
                self.simu_parameters["deep_or_up"]["plus_inf_function_rate"],
                self.simu_parameters["deep_or_up"]["moins_inf_function_rate"],
            )

        return choose_layer_shift

    def choose_layer_squared(self, time_event: float = 0) -> "function":
        """
        Cette fonction renvoie la fonction qui permet de choisir si les neurones 
        vont appartenir aux couches profondes ou au couches superficielles au carré. Il existe 
        un paramètre de shiftage car on va devoir souvent utiliser une fonction 
        shiftée lors de l'intégration par les mesures.

        Parameters
        ------------
        time_event : float
            Paramètre de shiftage. Ce shiftage correspond en fait au moment où
            l'évènement se produit.
        """

        def choose_layer_shift(
            time_division: float, time_second_division: float = 0
        ) -> float:
            """
            Cette fonction est la fonction qui permet de choisir si les neurones 
            vont appartenir aux couches profondes ou au couches superficielles au carré. Il existe 
            deux paramètre de shiftage correspondant aux instants de chacunes des divisions en 
            IPP/IPN car en fonction de quand ils ont lieu, la valeur de choose_layer 
            ne sera pas la même.

            Parameters
            ------------
            time_division : float
                Premier paramètre de shiftage. Ce shiftage correspond en fait au moment où
                dans le cas où elle se produit au moment de la division d'un IPP/IPN.

            time_division : float
                Premier paramètre de shiftage. Ce shiftage correspond en fait au moment où
                dans le cas où elle se produit au moment de la division d'un IPN qui 
                était précédemment un IPP.
            """
            return (
                1
                - sigmoide(
                    time_event + time_division + time_second_division,
                    self.simu_parameters["deep_or_up"]["slope_function_rate"],
                    self.simu_parameters["deep_or_up"]["location_function_rate"],
                    self.simu_parameters["deep_or_up"]["plus_inf_function_rate"],
                    self.simu_parameters["deep_or_up"]["moins_inf_function_rate"],
                )
            ) ** 2

        return choose_layer_shift

    def esperance(self, time: float) -> float:
        """
        Permet de caculer l'espérance du nombre de neurones appartenant aux
        couches superficielles en fonction du temps. Cette fonction obtient pour chaque 
        type d'évènement la fonction à intégrer à l'aide de leur mesure associée, 
        puis intègre à l'aide de chacune de ces mesures.

        Parameters
        ------------
        time : float
            Temps où on veut connaitre l'espérance.
        """

        # Coefficients devant chaque intégrale
        coeff_to_IPP = 4
        coeff_to_IPN = 2
        coeff_to_neurons = 1

        # Fonctions liées à chaque évènement utiles afin de calculer cette espérance
        function_IPP = lambda point: self.cycle_IPP.double_measure(
            time - point, self.cycle_IPN, self.choose_layer(point)
        )

        function_IPN = lambda point: self.cycle_IPN.measure(
            time - point, self.choose_layer(point)
        )

        function_neurons = self.choose_layer()

        coeffs_esp = [coeff_to_IPP, coeff_to_IPN, coeff_to_neurons]
        functions = [function_IPP, function_IPN, function_neurons]
        return self.events.combi_lin_measure(time, coeffs_esp, functions)

    def gradient_esperance(self, time: float) -> float:
        """
        Permet de caculer le gradient de l'espérance du nombre de neurones appartenant aux
        couches superficielles en fonction du temps. Cette fonction obtient pour chaque 
        type d'évènement la fonction à intégrer à l'aide de leur mesure associée, 
        puis intègre à l'aide de chacune de ces mesures.

        Parameters
        ------------
        time : float
            Temps où on veut connaitre le gradient de l'espérance.
        """
        # Coefficients devant chaque intégrale
        coeff_to_IPP = 4
        coeff_to_IPN = 2
        coeff_to_neurons = 1
        coeffs_esp = [coeff_to_IPP, coeff_to_IPN, coeff_to_neurons]

        # Liste fonction dont on va se servir afin d'intégrer
        density_first_var = lambda point: derivee_first_var_sig(
            point,
            self.simu_parameters["deep_or_up"]["slope_function_rate"],
            self.simu_parameters["deep_or_up"]["location_function_rate"],
            self.simu_parameters["deep_or_up"]["plus_inf_function_rate"],
            self.simu_parameters["deep_or_up"]["moins_inf_function_rate"],
        )
        density_second_var = lambda point: derivee_second_var_sig(
            point,
            self.simu_parameters["deep_or_up"]["slope_function_rate"],
            self.simu_parameters["deep_or_up"]["location_function_rate"],
            self.simu_parameters["deep_or_up"]["plus_inf_function_rate"],
            self.simu_parameters["deep_or_up"]["moins_inf_function_rate"],
        )
        density_third_var = lambda point: derivee_troisieme_var_sig(
            point,
            self.simu_parameters["deep_or_up"]["slope_function_rate"],
            self.simu_parameters["deep_or_up"]["location_function_rate"],
        )
        density_fourth_var = lambda point: derivee_quatrieme_var_sig(
            point,
            self.simu_parameters["deep_or_up"]["slope_function_rate"],
            self.simu_parameters["deep_or_up"]["location_function_rate"],
        )

        densities = [
            density_first_var,
            density_second_var,
            density_third_var,
            density_fourth_var,
        ]

        gradient = np.zeros(len(densities))
        # Fonctions liées à chaque évènement utiles afin de calculer cette espérance
        for k in range(0, len(gradient)):
            function_IPP = lambda point: self.cycle_IPP.double_measure(
                time - point, self.cycle_IPN, densities[k](point)
            )

            function_IPN = lambda point: self.cycle_IPN.measure(
                time - point, densities[k](point)
            )

            function_neurons = densities[k](0)

            functions = [function_IPP, function_IPN, function_neurons]

            gradient[k] = -self.events.combi_lin_measure(time, coeffs_esp, functions)

        return gradient

    def variance(self, time: float):
        """
        Permet de caculer la variance du nombre de neurones appartenant aux
        couches superficielles en fonction du temps. Cette fonction obtient pour chaque 
        type d'évènement la fonction à intégrer à l'aide de leur mesure associée, 
        puis intègre à l'aide de chacune de ces mesures.

        Parameters
        ------------
        time : float
            Temps où on veut connaitre la variance.
        """

        # Coefficients devant chaque intégrale
        coeff_to_IPP = 1
        coeff_to_IPN = 2
        coeff_to_neurons = 1

        # Fonction liées à chaque évènement utiles afin de calculer cette espérance
        coeff_func_IPP_1 = 8
        coeff_func_IPP_2 = 4

        function_IPP = lambda point: coeff_func_IPP_1 * self.cycle_IPP.double_measure_squared(
            time - point, self.cycle_IPN, self.choose_layer(point)
        ) + coeff_func_IPP_2 * (
            self.cycle_IPP.double_measure(
                time - point, self.cycle_IPN, self.choose_layer_squared(point)
            )
            + self.cycle_IPP.double_measure(
                time - point, self.cycle_IPN, self.choose_layer(point)
            )
        )

        function_IPN = lambda point: (
            self.cycle_IPN.measure(time - point, self.choose_layer_squared(point))
            + self.cycle_IPN.measure(time - point, self.choose_layer(point))
        )

        function_neurons = self.choose_layer()

        coeffs_var = [coeff_to_IPP, coeff_to_IPN, coeff_to_neurons]
        functions = [function_IPP, function_IPN, function_neurons]
        return self.events.combi_lin_measure(time, coeffs_var, functions)

    def ecart_type(self, time: float):
        """
        Permet de caculer l'écart-type en fonction du temps. Elle se sert 
        de la fonction liée au calcul de la variance pour cela.

        Parameters
        ------------
        time : float
            Temps où on veut connaitre l'écart-type.
        """
        return np.sqrt(self.variance(time))

    def update_layers_parameters(self, new_params_layers: "np.array"):
        """
        Cette fonction permet d'updater la valeurs des paramètres liés au choix
        des couches dans le fichier json, en mettant dans ceux-ci les valeurs 
        que l'on veut. Cela sera notamment utilisé lors de l'optimisation des 
        paramètres liés aux couches.

        Parameters
        ------------
        new_params_layers : "np.array"
            Valeurs des nouveaux paramètres liés au choix des couches qu'aura 
            notre fichier json.
        """
        self.simu_parameters["deep_or_up"]["slope_function_rate"] = new_params_layers[
            self.index_slope
        ]
        self.simu_parameters["deep_or_up"][
            "location_function_rate"
        ] = new_params_layers[self.index_location]
        self.simu_parameters["deep_or_up"][
            "plus_inf_function_rate"
        ] = new_params_layers[self.index_plus_inf]
        self.simu_parameters["deep_or_up"][
            "moins_inf_function_rate"
        ] = new_params_layers[self.index_moins_inf]

    def initialize_simu_parameters(self, type_sample: str, type_division: str):
        """ 
        Cette fonction permet d'initialiser le fichier json contenant les différents
        paramètres liés à notre optimisation. Dans un premier temps, on charge
        soit les paramètres du jeu de souris normale, soit les paramètres du jeu 
        de souris mutante puis on rajoute à ceux-ci le type de loi que l'on va 
        utiliser pour les durées de cycles cellulaires et les proportions des phases.

        Parameters
        ------------
        type_sample : str 
            Permet de choisir si nous allons plutôt prendre les paramètres pour la souris
            mutant ou pour la souris normale.

        type_division : str
            Si ce paramètre est à "constant", la neurogenèse se fera avec des temps 
            de cycle constant. Si il est à "gamma", les temps de cycles (ou phases) seront
            aléatoires et suivront une loi gamma dont les paramètres sont préalablement
            remplis dans le fichier parameters.json.
        """

        if type_sample == "Normal":
            simulation_parameters_file = "data/simulation/normal_parameters.json"
        if type_sample == "Mutant":
            simulation_parameters_file = "data/simulation/mutant_parameters.json"

        self.simu_parameters = json.load(open(simulation_parameters_file, "r"))
        self.events = Events(simulation_parameters_file)

        self.simu_parameters["type_division"] = type_division

    def initialize_type_division(self):
        """
        Cette fonction permet d'initialiser les lois des différents cycles
        associés à cette simulation. Les paramètres des différentes lois devront 
        être mis au préalable dans le fichier json.
        """

        if self.simu_parameters["type_division"] == "constant":
            self.cycle_IPP = Constant(self.simu_parameters["age_division"]["IPP"])
            self.cycle_IPN = Constant(self.simu_parameters["age_division"]["IPN"])

        if self.simu_parameters["type_division"] == "gamma":

            self.cycle_IPP = Gamma(
                simu_parameters["age_division"]["IPP"],
                simu_parameters["var_age_division"]["IPP"],
            )
            self.cycle_IPN = Gamma(
                simu_parameters["age_division"]["IPN"],
                simu_parameters["var_age_division"]["IPN"],
            )

        if self.simu_parameters["type_division"] == "exponential":

            self.cycle_IPP = Exponential(
                1 / self.simu_parameters["age_division"]["IPP"],
            )
            self.cycle_IPN = Exponential(
                1 / self.simu_parameters["age_division"]["IPN"],
            )

        if self.simu_parameters["type_division"] == "symetric_beta":

            self.cycle_IPP = SymetricBeta(
                self.simu_parameters["age_division"]["IPP"],
                self.simu_parameters["var_age_division"]["IPP"],
            )
            self.cycle_IPN = SymetricBeta(
                self.simu_parameters["age_division"]["IPN"],
                self.simu_parameters["var_age_division"]["IPN"],
            )
