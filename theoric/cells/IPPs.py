import numpy as np
import pandas as pd
import json
from theoric.measures import *
from utilities.functions.sigmoide import *
from utilities.utils import mean_var_cells


class IPPs(object):
    """
    Cette classe contient toutes les fonctions qui permettent de calculer
    l'espérance ainsi que la variance du nombre d'IPP en fonction du temps, en
    se servant des différentes mesures possibles.
    Attributes
    ------------
    type_sample : str 
        Permet de choisir si nous allons plutôt prendre les paramètres pour la souris
        mutant ou pour la souris normale.
    
    type_division : str
        Permet de choisir le type de loi que l'on va prendre pour le temps de division
        de chacun des types de cellules (gamma, constant etc...)
    """

    def __init__(
        self, type_sample: str = "Normal", type_division: str = "constant",
    ):

        self.initialize_simu_parameters(type_sample, type_division)
        self.initialize_type_division()

    def esperance(self, time: float) -> float:
        """
        Permet de caculer l'espérance du nombre d'IPP en fonction du temps. Cette 
        fonction obtient pour chaque type d'évènement la fonction à intégrer 
        à l'aide de leur mesure associée, puis intègre à l'aide de chacune de ces mesures.

        Parameters
        ------------
        time : float
            Temps où on veut connaitre l'espérance.
        """
        # Coefficients devant chaque intégrale
        coeff_to_IPP = 1
        coeff_to_IPN = 0
        coeff_to_neurons = 0

        # Fonctions liées à chaque évènement utiles afin de calculer cette espérance
        function_IPP = lambda point: 1 - self.cycle_IPP.cumul(time - point)

        function_IPN = lambda point: point  # Identité car de toute façon coeff 0 devant

        function_neurons = (
            lambda point: point
        )  # Identité car de toute façon coeff 0 devant

        coeffs_esp = [coeff_to_IPP, coeff_to_IPN, coeff_to_neurons]
        functions = [function_IPP, function_IPN, function_neurons]
        return self.events.combi_lin_measure(time, coeffs_esp, functions)

    def variance(self, time: float) -> float:
        """
        Permet de caculer la variance du nombre d'IPP en fonction du temps. Cette 
        fonction obtient pour chaque type d'évènement la fonction à intégrer 
        à l'aide de leur mesure associée, puis intègre à l'aide de chacune de ces mesures.

        Parameters
        ------------
        time : float
            Temps où on veut connaitre la variance.
        """
        # Coefficients devant chaque intégrale
        coeff_to_IPP = 1
        coeff_to_IPN = 0
        coeff_to_neurons = 0

        # Fonctions liées à chaque évènement utiles afin de calculer cette espérance
        function_IPP = lambda point: 1 - self.cycle_IPP.cumul(time - point)

        function_IPN = lambda point: point  # Identité car de toute façon coeff 0 devant

        function_neurons = (
            lambda point: point
        )  # Identité car de toute façon coeff 0 devant

        coeffs_esp = [coeff_to_IPP, coeff_to_IPN, coeff_to_neurons]
        functions = [function_IPP, function_IPN, function_neurons]
        return self.events.combi_lin_measure(time, coeffs_esp, functions)

    def ecart_type(self, time: float) -> float:
        """
        Permet de caculer l'écart-type du nombre d'IPP en fonction du temps. Cette 
        fonction se sert notamment de la fonction précédemment mise qui calcule la 
        variance au cours du temps.

        Parameters
        ------------
        time : float
            Temps où on veut connaitre l'écart-type.
        """
        return np.sqrt(self.variance(time))

    def initialize_type_division(self):
        """
        Cette fonction permet d'initialiser les lois des différents cycles
        associés à cette simulation. Les paramètres des différentes lois devront 
        être mis au préalable dans le fichier json.
        """

        if self.simu_parameters["type_division"] == "constant":
            self.cycle_IPP = Constant(self.simu_parameters["age_division"]["IPP"])
            self.cycle_IPN = Constant(self.simu_parameters["age_division"]["IPN"])

        if self.simu_parameters["type_division"] == "gamma":

            self.cycle_IPP = Gamma(
                self.simu_parameters["age_division"]["IPP"],
                self.simu_parameters["var_age_division"]["IPP"],
            )
            self.cycle_IPN = Gamma(
                self.simu_parameters["age_division"]["IPN"],
                self.simu_parameters["var_age_division"]["IPN"],
            )

        if self.simu_parameters["type_division"] == "exponential":

            self.cycle_IPP = Exponential(
                1 / self.simu_parameters["age_division"]["IPP"],
            )
            self.cycle_IPN = Exponential(
                1 / self.simu_parameters["age_division"]["IPN"],
            )

        if self.simu_parameters["type_division"] == "symetric_beta":

            self.cycle_IPP = SymetricBeta(
                self.simu_parameters["age_division"]["IPP"],
                self.simu_parameters["var_age_division"]["IPP"],
            )
            self.cycle_IPN = SymetricBeta(
                self.simu_parameters["age_division"]["IPN"],
                self.simu_parameters["var_age_division"]["IPN"],
            )

    def initialize_simu_parameters(self, type_sample: str, type_division: str):
        """ 
        Cette fonction permet d'initialiser le fichier json contenant les différents
        paramètres liés à notre optimisation. Dans un premier temps, on charge
        soit les paramètres du jeu de souris normale, soit les paramètres du jeu 
        de souris mutante puis on rajoute à ceux-ci le type de loi que l'on va 
        utiliser pour les durées de cycles cellulaires et les proportions des phases.

        Parameters
        ------------
        type_sample : str 
            Permet de choisir si nous allons plutôt prendre les paramètres pour la souris
            mutant ou pour la souris normale.

        type_division : str
            Si ce paramètre est à "constant", la neurogenèse se fera avec des temps 
            de cycle constant. Si il est à "gamma", les temps de cycles (ou phases) seront
            aléatoires et suivront une loi gamma dont les paramètres sont préalablement
            remplis dans le fichier parameters.json.
        """

        if type_sample == "Normal":
            simulation_parameters_file = "data/simulation/normal_parameters.json"
        if type_sample == "Mutant":
            simulation_parameters_file = "data/simulation/mutant_parameters.json"

        self.simu_parameters = json.load(open(simulation_parameters_file, "r"))
        self.events = Events(simulation_parameters_file)

        self.simu_parameters["type_division"] = type_division
