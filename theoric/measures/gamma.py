import scipy.integrate as integrate
import scipy.stats


class Gamma(object):
    """
    Cette classe contient un certain nombre de fonctions liées à la mesure de 
    probabilité d'une loi gamma associée à un couple (moyenne, variance). Elle 
    contient notamment des fonctions permettant d'avoir la fonction de répartition, 
    la densité, la mesure pour une fonction, la convolution etc.....

    Attributes
    ------------
    param_1_moyenne: float
        Moyenne de la loi gamma.

    param_2_variance: float
        Variance de la loi gamma.
    """

    def __init__(self, param_1_moyenne: float, param_2_variance: float):
        self.param_1_gamma = (param_1_moyenne ** 2) / param_2_variance
        self.param_2_gamma = param_2_variance / param_1_moyenne

    def realisation(self) -> float:
        """
        Cette fonction permet de donner une réalisation de la loi liée à cette 
        mesure de probabilité.
        """

        return scipy.stats.gamma.rvs(self.param_1_gamma, scale=self.param_2_gamma)

    def cumul(self, point: float,) -> float:
        """
        Cette fonction permet de donner la valeur de la fonction de répartition 
        de la loi liée à cette mesure de probabilité en un point donné.

        Parameters
        -------------
        point : float
            Point où on veut obtenir la fonction de répartition.
        """

        return scipy.stats.gamma.cdf(
            point, self.param_1_gamma, scale=self.param_2_gamma
        )

    def density(self, point: float,) -> float:
        """
        Cette fonction permet de donner la valeur de la densité de la loi liée
        à cette mesure de probabilité en un point donné.

        Parameters
        -------------
        point : float
            Point où on veut obtenir la densité.
        """

        return scipy.stats.gamma.pdf(
            point, self.param_1_gamma, scale=self.param_2_gamma
        )

    def measure(
        self, point_max: float, function: "function", point_min: float = 0
    ) -> float:
        """
        Cette fonction permet de donner la valeur de int_0^point f(x) mu(dx) c'est
        à dire la mesure de cette fonction pour les points inférieur au paramètre 
        de la fonction.

        Parameters
        -------------
        point_max : float
            Point maximum où nous allons essayer d'obtenir la mesure.
        
        function : "function"
            Fonction dont on veut obtenir la mesure.
    
        point_min : float
            Point minimum où nous allons essayer d'obtenir la mesure.
        """
        density_measure = lambda other_point: (
            self.density(other_point) * function(other_point)
        )

        return integrate.quad(density_measure, point_min, point_max)[0]

    def intervalle(self, borne_inf: float, borne_sup: float) -> float:
        """
        Cette fonction permet de donner la valeur de mu([a,b]) c'est-à-dire la 
        probabilité que la constante soit dans cet intervalle .

        Parameters
        -------------
        borne_inf : float
            Borne inférieure de l'intervalle.

        borne_sup : float
            Borne supérieure de l'intervalle.
        """
        return integrate.quad(self.density, borne_inf, borne_sup)[0]

    def double_measure(
        self, point: float, other_gamma: "class", fonction: "function"
    ) -> float:
        """
        Cette fonction permet de donner la valeur de int_0^point int_0^{point - x} f(x,y) mu_2(dy) mu_1(dx)
        c'est à dire la mesure de cette fonction à deux variables pour les points 
        dont la somme est inférieure au paramètre de la fonction.

        Parameters
        -------------
        point : float
            Point maximum où nous allons essayer d'obtenir la mesure.
        
        other_gamma: "class"
            L'autre mesure (liée à une loi gamma) que nous allons utiliser afin d'obtenir la double mesure.
        
        fonction : "function"
            Fonction dont on veut obtenir la mesure.
        """
        point_start = 0
        density_double_measure = lambda first_var, second_var: (
            self.density(first_var) * other_gamma.density(second_var)
        ) * fonction(first_var, second_var)
        return integrate.dblquad(
            density_double_measure,
            point_start,
            point,
            point_start,
            lambda first_var: point - first_var,
        )[0]

    def double_measure_squared(
        self, point: float, other_gamma: "class", fonction: "function"
    ) -> float:
        """
        Cette fonction permet de donner la valeur de int_0^point int_0^{point - x} f^2(x,y) mu_2(dy) mu_1(dx)
        c'est à dire la mesure de cette fonction au carré à deux variables pour les points 
        dont la somme est inférieure au paramètre de la fonction.

        Parameters
        -------------
        point : float
            Point maximum où nous allons essayer d'obtenir la double mesure.
        
        other_gamma: "class"
            L'autre mesure (liée à une loi gamma) que nous allons utiliser afin d'obtenir la double mesure.
        
        fonction : "function"
            Fonction dont on veut obtenir la double mesure de son carré.
        """
        point_start = 0
        density_double_measure_squared = (
            lambda first_var, second_var, third_var: (
                self.density(first_var)
                * other_gamma.density(second_var)
                * other_gamma.density(third_var)
            )
            * fonction(first_var, second_var)
            * fonction(first_var, third_var)
        )
        return integrate.tplquad(
            density_double_measure_squared,
            point_start,
            point,
            point_start,
            lambda first_var: point - first_var,
            point_start,
            lambda first_var, second_var: point - first_var,
        )[0]

    def triple_measure(
        self,
        point: float,
        other_gamma: "class",
        first_fonction: "function",
        second_fonction: "function",
    ) -> float:
        """
        Cette fonction permet de donner la valeur de:
        int_0^point int_0^{point - x} int_0^{point - x} f(x,y)f(x,z) mu_2 (dz)mu_2(dy) mu_1(dx).


        Parameters
        -------------
        point : float
            Point maximum où nous allons essayer d'obtenir la triple mesure.
        
        other_gamma: "class"
            L'autre mesure (liée à une loi gamma) que nous allons utiliser afin d'obtenir la triple mesure.
        
        fonction : "function"
            Fonction dont on veut obtenir la triple mesure.
        """
        point_start = 0
        density_double_measure_squared = (
            lambda first_var, second_var, third_var: (
                self.density(first_var)
                * other_gamma.density(second_var)
                * other_gamma.density(third_var)
            )
            * first_fonction(first_var, second_var)
            * second_fonction(first_var, third_var)
        )
        return integrate.tplquad(
            density_double_measure_squared,
            point_start,
            point,
            point_start,
            lambda first_var: point - first_var,
            point_start,
            lambda first_var, second_var: point - first_var,
        )[0]

    def convolve(self, point: float, function: "function") -> float:
        """
        Cette fonction permet de donner la valeur de int_0^point f(point-x) mu(dx) c'est
        à dire la convolution de la fonction f par rapport à la mesure mu.

        Parameters
        -------------
        point : float
            Point où nous allons essayer d'obtenir la convolution.
        
        fonction : "function"
            Fonction dont on veut obtenir la convolution.
        """
        point_start = 0
        density_convol = lambda other_point: (
            self.density(other_point) * function(point - other_point)
        )

        return integrate.quad(density_convol, point_start, point)[0]

    def convolve_squared_cumul(self, point: float, other_gamma: "class",) -> float:
        """
        Cette fonction permet de donner la valeur de int_0^point f(point-x)^2 mu(dx) 
        c'est où f est la fonction de répartition liée à une certaine mesure de probabilité.

        Parameters
        -------------
        point : float
            Point où nous allons essayer d'obtenir la convolution.
        
        other_gamma: "class"
            Mesure de probablité (gamma) dont on veut obtenir la convolution
            de sa fonction de répartition au carré.
        """
        cumul_squared = lambda point: other_gamma.cumul(point) ** 2
        return self.convolve(point, cumul_squared)

    def convolve_squared_tail(self, point: float, other_gamma: "class") -> float:
        """
        Cette fonction permet de donner la valeur de int_0^point f(point-x)^2 mu(dx) 
        c'est où f est la queue liée à une certaine mesure de probabilité.

        Parameters
        -------------
        point : float
            Point maximum où nous allons essayer d'obtenir la convolution.
        
        other_constant: "class"
            Mesure de probablité (gamma) dont on veut obtenir la convolution
            de sa queue au carré.
        """
        tail_squared = lambda point: (1 - other_gamma.cumul(point)) ** 2
        return self.convolve(point, tail_squared)
