import scipy.integrate as integrate
import scipy.stats


class SymetricBeta(object):
    """
    Cette classe contient un certain nombre de fonctions liées à la mesure de 
    probabilité d'une loi beta symétrique renormalisée associée à un couple 
    (moyenne, variance). Elle contient notamment des fonctions permettant d'avoir 
    la fonction de répartition, la densité, la mesure pour une fonction, la 
    convolution etc.....

    Attributes
    ------------
    param_1_moyenne: float
        Moyenne de la loi beta.

    param_2_variance: float
        Variance de la loi beta.
    """

    def __init__(self, param_1_moyenne: float, param_2_variance: float):
        self.param_1_sym_beta = (param_1_moyenne ** 2 - param_2_variance) / (
            2 * param_2_variance
        )
        self.param_2_sym_beta = 2 * param_1_moyenne

    def realisation(self) -> float:
        """
        Cette fonction permet de donner une réalisation de la loi liée à cette 
        mesure de probabilité.
        """

        return (
            scipy.stats.beta.rvs(self.param_1_sym_beta, self.param_1_sym_beta)
            * self.param_2_sym_beta
        )

    def cumul(self, point: float,) -> float:
        """
        Cette fonction permet de donner la valeur de la fonction de répartition 
        de la loi liée à cette mesure de probabilité en un point donné.

        Parameters
        -------------
        point : float
            Point où on veut obtenir la fonction de répartition.
        """

        return (
            scipy.stats.beta.cdf(point, self.param_1_sym_beta, self.param_1_sym_beta,)
            / self.param_2_sym_beta
        )

    def density(self, point: float,) -> float:
        """
        Cette fonction permet de donner la valeur de la densité de la loi liée
        à cette mesure de probabilité en un point donné.

        Parameters
        -------------
        point : float
            Point où on veut obtenir la densité.
        """

        return (
            scipy.stats.beta.pdf(point, self.param_1_sym_beta, self.param_1_sym_beta,)
            / self.param_2_sym_beta
        )

    def measure(self, point: float, function: "function"):
        """
        Cette fonction permet de donner la valeur de int_0^point f(x) mu(dx) c'est
        à dire la mesure de cette fonction pour les points inférieur au paramètre 
        de la fonction.

        Parameters
        -------------
        point : float
            Point maximum où nous allons essayer d'obtenir la mesure.
        
        fonction : "function"
            Fonction dont on veut obtenir la mesure.
        """
        point_start = 0
        density_measure = lambda other_point: (
            self.density(other_point) * function(other_point)
        )

        return integrate.quad(density_measure, point_start, point)[0]

    def double_measure(
        self, point: float, other_beta: "class", fonction: "function"
    ) -> float:
        """
        Cette fonction permet de donner la valeur de int_0^point int_0^{point - x} f(x,y) mu_2(dy) mu_1(dx)
        c'est à dire la mesure de cette fonction à deux variables pour les points 
        dont la somme est inférieure au paramètre de la fonction.

        Parameters
        -------------
        point : float
            Point maximum où nous allons essayer d'obtenir la mesure.
        
        other_beta: "class"
            L'autre mesure (liée à une loi beta) que nous allons utiliser afin d'obtenir la double mesure.
        
        fonction : "function"
            Fonction dont on veut obtenir la mesure.
        """
        point_start = 0
        density_double_measure = lambda first_var, second_var: (
            self.density(first_var) * other_beta.density(second_var)
        ) * fonction(first_var, second_var)
        return integrate.dblquad(
            density_double_measure,
            point_start,
            point,
            point_start,
            lambda first_var: point - first_var,
        )[0]

    def double_measure_squared(
        self, point: float, other_beta: "class", fonction: "function"
    ) -> float:
        """
        Cette fonction permet de donner la valeur de int_0^point int_0^{point - x} f^2(x,y) mu_2(dy) mu_1(dx)
        c'est à dire la mesure de cette fonction au carré à deux variables pour les points 
        dont la somme est inférieure au paramètre de la fonction.

        Parameters
        -------------
        point : float
            Point maximum où nous allons essayer d'obtenir la double mesure.
        
        other_beta: "class"
            L'autre mesure (liée à une loi beta) que nous allons utiliser afin d'obtenir la double mesure.
        
        fonction : "function"
            Fonction dont on veut obtenir la double mesure de son carré.
        """
        point_start = 0
        density_double_measure_squared = (
            lambda first_var, second_var, third_var: (
                self.density(first_var)
                * other_beta.density(second_var)
                * other_beta.density(third_var)
            )
            * fonction(first_var, second_var)
            * fonction(first_var, third_var)
        )
        return integrate.tplquad(
            density_double_measure_squared,
            point_start,
            point,
            point_start,
            lambda first_var: point - first_var,
            point_start,
            lambda first_var, second_var: point - first_var,
        )[0]

    def triple_measure(
        self,
        point: float,
        other_beta: "class",
        first_fonction: "function",
        second_fonction: "function",
    ) -> float:
        """
        Cette fonction permet de donner la valeur de:
        int_0^point int_0^{point - x} int_0^{point - x} f(x,y)f(x,z) mu_2 (dz)mu_2(dy) mu_1(dx).


        Parameters
        -------------
        point : float
            Point maximum où nous allons essayer d'obtenir la triple mesure.
        
        other_beta: "class"
            L'autre mesure (liée à une loi beta) que nous allons utiliser afin d'obtenir la triple mesure.
        
        fonction : "function"
            Fonction dont on veut obtenir la triple mesure.
        """
        point_start = 0
        density_double_measure_squared = (
            lambda first_var, second_var, third_var: (
                self.density(first_var)
                * other_beta.density(second_var)
                * other_beta.density(third_var)
            )
            * first_fonction(first_var, second_var)
            * second_fonction(first_var, third_var)
        )
        return integrate.tplquad(
            density_double_measure_squared,
            point_start,
            point,
            point_start,
            lambda first_var: point - first_var,
            point_start,
            lambda first_var, second_var: point - first_var,
        )[0]

    def convolve(self, point: float, function: "function") -> float:
        """
        Cette fonction permet de donner la valeur de int_0^point f(point-x) mu(dx) c'est
        à dire la convolution de la fonction f par rapport à la mesure mu.

        Parameters
        -------------
        point : float
            Point où nous allons essayer d'obtenir la convolution.
        
        fonction : "function"
            Fonction dont on veut obtenir la convolution.
        """
        point_start = 0
        density_convol = lambda other_point: (
            self.density(other_point) * function(point - other_point)
        )

        return integrate.quad(density_convol, point_start, point)[0]

    def convolve_squared_cumul(self, point: float, other_beta: "class",) -> float:
        """
        Cette fonction permet de donner la valeur de int_0^point f(point-x)^2 mu(dx) 
        c'est où f est la fonction de répartition liée à une certaine mesure de probabilité.

        Parameters
        -------------
        point : float
            Point où nous allons essayer d'obtenir la convolution.
        
        other_beta: "class"
            Mesure de probablité (beta) dont on veut obtenir la convolution
            de sa fonction de répartition au carré.
        """
        cumul_squared = lambda point: other_beta.cumul(point) ** 2
        return self.convolve(point, cumul_squared)

    def convolve_squared_tail(self, point: float, other_beta: "class") -> float:
        """
        Cette fonction permet de donner la valeur de int_0^point f(point-x)^2 mu(dx) 
        c'est où f est la queue liée à une certaine mesure de probabilité.

        Parameters
        -------------
        point : float
            Point maximum où nous allons essayer d'obtenir la convolution.
        
        other_constant: "class"
            Mesure de probablité (beta) dont on veut obtenir la convolution
            de sa queue au carré.
        """
        tail_squared = lambda point: (1 - other_beta.cumul(point)) ** 2
        return self.convolve(point, tail_squared)
