import scipy.integrate as integrate
import json
from utilities.functions.rates import *


class Events(object):
    """
    Cette classe est utile afin de pouvoir calculer la mesure d'une fonction 
    pour chaque mesure liées aux évènements:

    AP -> IPP
    AP -> IPN
    AP -> N.

    Elle permet également de faire une combinaison linéaire de ces mesures. Cette
    classe sera appelée durant le calcule de chaque espérance/variance.

    Attributes
    ------------
    parameters_file : str
        Nom du fichier qui contient les informations sur les valeurs des paramètres
        liées aux fonctions de taux pour chaque évènement.   
    """

    def __init__(self, parameters_file: str):
        self.params = json.load(open(parameters_file, "r"))

    def measure_AP_to_IPP(
        self, time: float, function: "function", points: list = None
    ) -> float:
        """ 
        Cette fonction permet de calculer la mesure d'une fonction, pour la mesure
        liée à l'évènement AP -> IPP (c'est-à-dire gamma(s)beta(s)ds), et pour les 
        points inférieurs à un certain temps, c'est-à-dire obtenir pour une fonction 
        f la valeur de :

        int_0^t f(s)gamma(s)beta(s)ds.

        Parameters
        ------------
        time : float 
            Temps maximal, on veut la mesure pour les points inférieurs à ce temps.
        
        function : "function"
            Fonction dont on veut obtenir la mesure.
        
        points: list
            Endroit où est localisée la fonction. Est utile lorsque la fonction 
            a trop de trous car l'intégrale ne pourra pas être calculée si on ne 
            donne pas où est située la fonction.
        """
        time_start = 0
        index_integral_val = 0  # La fonction integrate.quad renvoie 2 valeurs, celle à l'indice 0 s'agit de la valeur de l'intégrale.
        density = (
            lambda time: rate_function_IPP(time, self.params)
            * rate_AP(time, self.params)
            * function(time)
        )

        return integrate.quad(density, time_start, time, points=points)[
            index_integral_val
        ]

    def measure_AP_to_IPN(self, time: float, function: "function") -> float:
        """ 
        Cette fonction permet de calculer la mesure d'une fonction, pour la mesure
        liée à l'évènement AP -> IPN (c'est-à-dire (1-gamma(s))beta(s)ds), et pour les 
        points inférieurs à un certain temps, c'est-à-dire obtenir pour une fonction 
        f la valeur de :

        int_0^t f(s)(1-gamma(s))beta(s)ds.

        Parameters
        ------------
        time : float 
            Temps maximal, on veut la mesure pour les points inférieurs à ce temps.
        
        function : "function"
            Fonction dont on veut obtenir la mesure.
        """
        time_start = 0
        index_integral_val = 0
        density = (
            lambda time: rate_function_IPN(time, self.params)
            * rate_AP(time, self.params)
            * function(time)
        )
        return integrate.quad(density, time_start, time)[index_integral_val]

    def measure_AP_to_neurons(self, time: float, function: "function") -> float:
        """ 
        Cette fonction permet de calculer la mesure d'une fonction, pour la mesure
        liée à l'évènement AP -> N (c'est-à-dire (1-beta(s))ds, et pour les 
        points inférieurs à un certain temps, c'est-à-dire obtenir pour une fonction 
        f la valeur de :

        int_0^t f(s)(1-beta(s))ds.

        Parameters
        ------------
        time : float 
            Temps maximal, on veut la mesure pour les points inférieurs à ce temps.
        
        function : "function"
            Fonction dont on veut obtenir la mesure.
        """
        time_start = 0
        index_integral_val = 0

        density = (
            lambda time: rate_function_neurons(time, self.params)
            * rate_AP(time, self.params)
            * function(time)
        )
        return integrate.quad(density, time_start, time)[index_integral_val]

    def combi_lin_measure(
        self,
        time: float,
        coeffs: list,
        functions: "list[function]",
        points_IPP: list = None,
    ) -> float:
        """ 
        Cette fonction permet de calculer la combinaison linéaire de mesures de 
        3 fonctions par la mesure de chacun des trois évènements, et pour des points
        inférieur à un certain temps. Concrètement, nous voulons à l'aide de cette 
        fonction obtenir:

        C_1int_0^t f_1(s)gamma(s)beta(s)ds + C_2int_0^t f_2(s)(1-gamma(s))beta(s)ds 
        + C_3int_0^t f_3(s)(1-beta(s))ds.


        Parameters
        ------------
        time : float 
            Temps maximal, on veut la mesure pour les points inférieurs à ce temps.

        coeffs : list
            Coefficients devant chaque intégrale.    
        
        function : "function"
            Fonction dont on veut obtenir la mesure.
    

        points_IPP: list
            Endroit où est localisée la fonction. Est utile lorsque la fonction 
            a trop de trous, ce qui arrive par exemple lors des calculs des 
            espérances des phases.
        """
        index_to_IPP = 0
        index_to_IPN = 1
        index_to_neurons = 2

        return (
            coeffs[index_to_IPP]
            * self.measure_AP_to_IPP(time, functions[index_to_IPP], points=points_IPP)
            + coeffs[index_to_IPN]
            * self.measure_AP_to_IPN(time, functions[index_to_IPN])
            + coeffs[index_to_neurons]
            * self.measure_AP_to_neurons(time, functions[index_to_neurons])
        )

