import numpy as np


class Constant(object):
    """
    Cette classe contient un certain nombre de fonctions liées à la mesure de 
    probabilité constante (un dirac). Elle contient notamment des fonctions permettant
    d'avoir la fonction de répartition, la densité, la mesure pour une fonction, la convolution
    etc.....

    Attributes
    ------------
    constant : float
        Valeur de la constante liée à cette mesure de probabilité.
    """

    def __init__(self, constant: float):
        self.constant = constant

    def realisation(self) -> float:
        """
        Cette fonction permet de donner une réalisation de la loi liée à cette 
        mesure de probabilité.
        """
        return self.constant

    def cumul(self, point: float,) -> float:
        """
        Cette fonction permet de donner la valeur de la fonction de répartition 
        de la loi liée à cette mesure de probabilité en un point donné.

        Parameters
        -------------
        point : float
            Point où on veut obtenir la fonction de répartition.
        """
        return (point > self.constant) * 1

    def density(self, point: float,) -> float:
        """
        Cette fonction permet de donner la valeur de la densité de la loi liée
        à cette mesure de probabilité en un point donné.

        Parameters
        -------------
        point : float
            Point où on veut obtenir la densité.
        """
        return (point == self.constant) * 1

    def measure(
        self, point_max: float, fonction: "function", point_min: float = 0
    ) -> float:
        """
        Cette fonction permet de donner la valeur de int_0^point f(x) mu(dx) c'est
        à dire la mesure de cette fonction pour les points inférieur au paramètre 
        de la fonction.

        Parameters
        -------------
        point_max : float
            Point maximum où nous allons essayer d'obtenir la mesure.
        
        fonction : "function"
            Fonction dont on veut obtenir la mesure.
    
        point_min : float
            Point minimum où nous allons essayer d'obtenir la mesure.
        """
        return fonction(self.constant) * (
            (point_max > self.constant) & (point_min <= self.constant)
        )

    def intervalle(self, borne_inf: float, borne_sup: float) -> float:
        """
        Cette fonction permet de donner la valeur de mu([a,b]) c'est-à-dire la 
        probabilité que la constante soit dans cet intervalle (vaut 0 ou 1 ici).

        Parameters
        -------------
        borne_inf : float
            Borne inférieure de l'intervalle.

        borne_sup : float
            Borne supérieure de l'intervalle.
        """
        return (borne_sup > self.constant) & (borne_inf <= self.constant) * 1

    def double_measure(
        self, point: float, other_constant: "class", fonction: "function"
    ) -> float:
        """
        Cette fonction permet de donner la valeur de int_0^point int_0^{point - x} f(x,y) mu_2(dy) mu_1(dx)
        c'est à dire la mesure de cette fonction à deux variables pour les points 
        dont la somme est inférieure au paramètre de la fonction.

        Parameters
        -------------
        point : float
            Point maximum où nous allons essayer d'obtenir la mesure.
        
        other_constant: "class"
            L'autre mesure que nous allons utiliser afin d'obtenir la double mesure.
        
        fonction : "function"
            Fonction dont on veut obtenir la mesure.
        """
        return (fonction(self.constant, other_constant.constant)) * (
            point > self.constant + other_constant.constant
        )

    def double_measure_squared(
        self, point: float, other_constant: "class", fonction: "function"
    ) -> float:
        """
        Cette fonction permet de donner la valeur de int_0^point int_0^{point - x} f^2(x,y) mu_2(dy) mu_1(dx)
        c'est à dire la mesure de cette fonction au carré à deux variables pour les points 
        dont la somme est inférieure au paramètre de la fonction.

        Parameters
        -------------
        point : float
            Point maximum où nous allons essayer d'obtenir la double mesure.
        
        other_constant: "class"
            L'autre mesure que nous allons utiliser afin d'obtenir la double mesure.
        
        fonction : "function"
            Fonction dont on veut obtenir la double mesure de son carré.
        """
        return (fonction(self.constant, other_constant.constant) ** 2) * (
            point > self.constant + other_constant.constant
        )

    def triple_measure(
        self,
        point: float,
        other_constant: "class",
        first_fonction: "function",
        second_fonction: "function",
    ) -> float:
        """
        Cette fonction permet de donner la valeur de:
         int_0^point int_0^{point - x} int_0^{point - x} f(x,y)f(x,z) mu_2 (dz)mu_2(dy) mu_1(dx).


        Parameters
        -------------
        point : float
            Point maximum où nous allons essayer d'obtenir la triple mesure.
        
        other_constant: "class"
            L'autre mesure que nous allons utiliser afin d'obtenir la triple mesure.
        
        fonction : "function"
            Fonction dont on veut obtenir la triple mesure.
        """
        return (
            first_fonction(self.constant, other_constant.constant)
            * second_fonction(self.constant, other_constant.constant)
        ) * (point > self.constant + other_constant.constant)

    def convolve(self, point: float, function: float) -> float:
        """
        Cette fonction permet de donner la valeur de int_0^point f(point-x) mu(dx) c'est
        à dire la convolution de la fonction f par rapport à la mesure mu.

        Parameters
        -------------
        point : float
            Point où nous allons essayer d'obtenir la convolution.
        
        fonction : "function"
            Fonction dont on veut obtenir la convolution.
        """
        return function(point - self.constant) * (point > self.constant)

    def convolve_squared_cumul(self, point: float, other_constant: "class",) -> float:
        """
        Cette fonction permet de donner la valeur de int_0^point f(point-x)^2 mu(dx) 
        c'est où f est la fonction de répartition liée à une certaine mesure de probabilité.

        Parameters
        -------------
        point : float
            Point où nous allons essayer d'obtenir la convolution.
        
        other_constant: "class"
            Mesure de probablité (constante) dont on veut obtenir la convolution
            de sa fonction de répartition au carré.
        """
        return (point > self.constant + other_constant.constant) * 1

    def convolve_squared_tail(self, point: float, other_constant: "class") -> float:
        """
        Cette fonction permet de donner la valeur de int_0^point f(point-x)^2 mu(dx) 
        c'est où f est la queue liée à une certaine mesure de probabilité.

        Parameters
        -------------
        point : float
            Point maximum où nous allons essayer d'obtenir la convolution.
        
        other_constant: "class"
            Mesure de probablité (constante) dont on veut obtenir la convolution
            de sa queue au carré.
        """
        return (
            (point > self.constant)
            * (point < self.constant + other_constant.constant)
            * 1
        )

