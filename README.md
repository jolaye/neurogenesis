# Neurogenesis
Ce dépôt contient un programme permettant  de simuler la neurogenèse à l'aide d'un algorithme de simulation exacte. L'algorithme utilisé est la version Extrande de l'algorithme Gillespie qui permet de prendre en compte les taux qui dépendent du temps. Le modèle sur lequel est basé ce programme:

* prend en compte 3 compartiments : IPP, IPN et neurones,
* permet d'avoir des temps de cycles constants ou aléatoires,
* peut si on le souhaite séparer les cycles phase par phase, avec des proportions de phases constantes ou aléatoires,
* prend en compte le fait que les neurones peuvent être séparés en différentes couches.

On peut si on le souhaite comparer nos simulations avec les simulations faites avec un modèle déterministe, mis au point par Marie Postel dans l'article "A multiscale mathematical model of cell dynamics during neurogenesis in the mouse cerebral cortex". Le code dans le dossier "deterministic" est à 95% codé par elle.

Il y a également des fonctions d'optimisation qui permettent notamment d'optimiser les paramètres liés aux couches. Cependant, le principal intérêt du programme est surtout de pouvoir simuler la neurogenèse.

## Setup

Afin d'installer les différents packages requis pour faire fonctionner ce programme, on peut se servir du fichier "requirements.txt".

```bash
pip install --user -r requirements.txt
```

Afin d'installer le programme sous forme de package local, on peut lancer la commande suivante (nécessaire de lancer afin de faire fonctionner les scripts).

```bash
pip install --user -e .
```


## Simulation de la neurogenèse

On peut lancer la simulation de plusieurs neurogenèse de deux manières différentes. Soit on peut utiliser la commande suivante qui va permettre de tracer 100 simulations de la neurogenèse (le nombre de simulations peut être modifié dans le fichier makefile):

```bash
make demo
```

soit on se sert du script "./scripts/plot_simuls.py":

```bash
python ./scripts/plot_simuls.py
```

Cette deuxième méthode permet de spécifier plus finement les options que l'on veut utiliser lors de nos simulations. Par exemple, si nous voulons cette fois-ci tracer 1000 simulations (cela peut prendre quelques dizaines de minutes), pour des durées de cycles aléatoires suivant une loi beta et afficher la moyenne en plus de toutes les courbes, il nous suffit de spécifier les options suivantes lorsque l'on lance notre script:

```bash
python ./scripts/plot_simuls.py -n 1000 -p "beta" -m 1
```

Les informations sur toutes les options que nous pouvons utiliser sont disponibles à l'aide de la commande: 
```bash
python ./scripts/plot_simuls.py --help
```
