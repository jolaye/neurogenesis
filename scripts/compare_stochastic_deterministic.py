# En lançant ce script, nous pouvons comparer le modèle stochastique et le modèle
# déterministe.

import click
from utilities.plot.plot_simulation import compare_stochastic_deterministic


@click.command()
@click.option(
    "--n_simuls",
    "-n",
    type=int,
    default=200,
    help="Nombre de simulations à afficher pour la simulation stochastique.",
)
@click.option(
    "--mean_compare",
    "-m",
    type=bool,
    default=False,
    help="Permet de savoir si nous allons effectuer une comparaison avec la moyenne du modèle stochastique ou alors toutes les courbes.",
)
@click.option(
    "--sample",
    "-s",
    type=str,
    default="Normal",
    help="Permet de savoir si nous allons prendre les paramètres de simulations de la souris mutante ou de la souris normale.",
)
def compare_stocha_deter(n_simuls: int, mean_compare: bool, sample: str):

    compare_stochastic_deterministic(n_simuls, mean_compare, sample)


if __name__ == "__main__":

    compare_stocha_deter()
