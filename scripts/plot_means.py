# Ce script permet de lancer plusieurs simulations et d'afficher leur moyenne.

import click
from utilities.functions.moments import mean_cells
from utilities.plot.plot_simulation import plot_cells, plot_several_simuls


@click.command()
@click.option(
    "--n_simuls",
    "-n",
    type=int,
    default=60,
    help="Nombre de simulations que l'on va effectuer afin de calculer la moyenne.",
)
@click.option(
    "--cycle",
    "-c",
    type=str,
    default="gamma",
    help="Nom de la loi que nous allons utiliser pour les cycles.",
)
@click.option(
    "--phase",
    "-p",
    type=str,
    default="constant",
    help="Nom de la loi que nous allons utiliser pour les phases.",
)
@click.option(
    "--sample",
    "-s",
    type=str,
    default="Normal",
    help="Permet de savoir si nous allons prendre les paramètres de simulations de la souris mutante ou de la souris normale.",
)
@click.option(
    "--all_curves",
    "-a",
    default=True,
    type=bool,
    help="Permet de savoir si nous allons tracer toutes les courbes ou non.",
)
@click.option(
    "--by_phase",
    "-b",
    type=bool,
    default=False,
    help="Permet de savoir si nous allons tracer les phases également ou non.",
)
def plot_means(
    n_simuls: int,
    cycle: str,
    phase: str,
    sample: str,
    all_curves: bool,
    by_phase: bool,
):

    times, means, all_cells = mean_cells(
        cycle, phase, sample, with_all_cells=True, n_simuls=n_simuls
    )

    if all_curves:
        plot_several_simuls(times, all_cells, means, by_phase)

    else:
        plot_cells(times, means, by_phase)


if __name__ == "__main__":

    plot_means()
