# En lançant ce script, nous pouvons comparer les données expérimentales avec
# les valeurs théoriques.

import click
from utilities.plot.plot_theoric import compare_theo_exp


@click.command()
@click.option(
    "--sample",
    "-s",
    type=str,
    default="Normal",
    help="Permet de savoir si nous allons prendre les paramètres de simulations de la souris mutante ou de la souris normale.",
)
@click.option(
    "--cycle",
    "-c",
    type=str,
    default="constant",
    help="Nom de la loi que nous allons utiliser pour les cycles.",
)
def compare_theo_experimental(sample: str, cycle: str):

    type_cells = ["IPP", "IPN", "N", "DL", "UL"]
    for type_cell in type_cells:
        compare_theo_exp(type_cell, sample, cycle)


if __name__ == "__main__":

    compare_theo_experimental()
