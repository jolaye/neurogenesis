# En lançant ce script, nous pouvons comparer les données expérimentales avec
# les simulations.

import click
from utilities.plot.plot_theoric import compare_theo_simul


@click.command()
@click.option(
    "--moment",
    "-m",
    type=str,
    default="Esperance",
    help="Nom du moment (Esperance/Variance) que nous voulons calculer ici.",
)
@click.option(
    "--sample",
    "-s",
    type=str,
    default="Normal",
    help="Permet de savoir si nous allons prendre les paramètres de simulations de la souris mutante ou de la souris normale.",
)
@click.option(
    "--cycle",
    "-c",
    type=str,
    default="constant",
    help="Nom de la loi que nous allons utiliser pour les cycles.",
)
def compare_theo_simulation(moment: str, sample: str, cycle: str):

    type_cells = ["IPP", "IPN", "N", "DL", "UL"]
    for type_cell in type_cells:
        compare_theo_simul(type_cell, moment, sample, cycle)


if __name__ == "__main__":

    compare_theo_simulation()
