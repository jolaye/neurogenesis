# Nous pouvons effectuer n'importe quel optimisation en lançant ce script.

import numpy as np
import click
from utilities.plot.plot_optimisation import opt_and_plot


@click.command()
@click.option(
    "--function",
    "-f",
    type=str,
    default="layers",
    help="Nom de la fonction que l'on cherche à optimiser.",
)
@click.option(
    "--method",
    "-m",
    type=str,
    default="genetic",
    help="Nom de la méthode d'optimisation que l'on va utiliser.",
)
@click.option(
    "--param",
    "-p",
    type=float,
    multiple=True,
    default=None,
    help="Paramètres initiaux pour notre optimisation.",
)
def plot_opti(function: str, method: str, param: float):
    param_init = np.array(param, dtype=float)
    opt_and_plot(function, method, param_init)


if __name__ == "__main__":

    plot_opti()
