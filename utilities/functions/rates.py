# Permet de calculer les taux des différents évènements.
from utilities.functions.sigmoide import sigmoide
import json


def rate_AP(time: float, parameters: "np.array"):
    """
    Cette fonction permet de calculer le taux auquel les progéniteurs apicaux
    rentrent dans la neurogenèse pour un temps donné. Ce taux est calculé avec 
    une multiplication de deux fonctions sigmoides permettant d'abord une phase 
    ascendante et ensuite une phase descendante.

    Parameters
    ------------
    time : float 
        Temps pour lequel nous voulons connaître le taux auquel les progéniteurs
        apicaux rentrent dans la neurogenèse.

    parameters : "np.array"
        Tableau numpy qui comporte la valeur de tous les paramètres liés à cette 
        fonction de taux.
    """
    return (
        parameters["AP_enter_neuro"]["scaling_factor"]
        * (
            1
            - sigmoide(
                time,
                parameters["AP_enter_neuro"]["slope_inflexion_rise"],
                parameters["AP_enter_neuro"]["location_inflexion_rise"],
            )
        )
        * sigmoide(
            time,
            parameters["AP_enter_neuro"]["slope_inflexion_decay"],
            parameters["AP_enter_neuro"]["location_inflexion_decay"],
        )
    )


def rate_function_neurons(time: float, parameters: "np.array") -> float:
    """
    Cette fonction permet de calculer le taux auquel arrive la division 
    AP -> AP + N. On utilise pour la calculer une fonction sigmoide (comme 
    pour les autres fonctions de ce type). 

    Parameters
    ------------
    time: float
        Temps auquel on veut calculer le taux où on a la division AP -> AP + N.
    
    parameters : "np.array"
        Tableau numpy qui comporte la valeur de tous les paramètres liés à cette 
        fonction de taux.
    """
    return 1 - sigmoide(
        time,
        parameters["AP_to_IP"]["slope_function_rate"],
        parameters["AP_to_IP"]["location_function_rate"],
        parameters["AP_to_IP"]["plus_inf_function_rate"],
        parameters["AP_to_IP"]["moins_inf_function_rate"],
    )


def rate_function_IPN(time: float, parameters: "np.array") -> float:
    """
    Cette fonction permet de calculer le taux auquel arrive la division 
    AP -> AP + IPN. On utilise pour la calculer une fonction sigmoide (comme 
    pour les autres fonctions de ce type). 

    Parameters
    ------------
    time: float
        Temps auquel on veut calculer le taux où on a la division AP -> AP + IPN.

    parameters : "np.array"
        Tableau numpy qui comporte la valeur de tous les paramètres liés à cette 
        fonction de taux.
    """
    return sigmoide(
        time,
        parameters["AP_to_IP"]["slope_function_rate"],
        parameters["AP_to_IP"]["location_function_rate"],
        parameters["AP_to_IP"]["plus_inf_function_rate"],
        parameters["AP_to_IP"]["moins_inf_function_rate"],
    ) * (
        1
        - sigmoide(
            time,
            parameters["IPP_or_IPN"]["slope_function_rate"],
            parameters["IPP_or_IPN"]["location_function_rate"],
            parameters["IPP_or_IPN"]["plus_inf_function_rate"],
            parameters["IPP_or_IPN"]["moins_inf_function_rate"],
        )
    )


def rate_function_IPP(time: float, parameters: "np.array") -> float:
    """
    Cette fonction permet de calculer le taux auquel arrive la division 
    AP -> AP + IPP. On utilise pour la calculer une fonction sigmoide (comme 
    pour les autres fonctions de ce type). 
    Parameters
    ------------
    time: float
        Temps auquel on veut calculer le taux où on a la division AP -> AP + IPP.

    parameters : "np.array"
        Tableau numpy qui comporte la valeur de tous les paramètres liés à cette 
        fonction de taux.
    """
    return sigmoide(
        time,
        parameters["AP_to_IP"]["slope_function_rate"],
        parameters["AP_to_IP"]["location_function_rate"],
        parameters["AP_to_IP"]["plus_inf_function_rate"],
        parameters["AP_to_IP"]["moins_inf_function_rate"],
    ) * (
        sigmoide(
            time,
            parameters["IPP_or_IPN"]["slope_function_rate"],
            parameters["IPP_or_IPN"]["location_function_rate"],
            parameters["IPP_or_IPN"]["plus_inf_function_rate"],
            parameters["IPP_or_IPN"]["moins_inf_function_rate"],
        )
    )
