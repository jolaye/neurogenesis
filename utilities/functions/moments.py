# Fonctions utiles afin de regarder la moyenne/variance de chaque type de cellules
# après avoir effectué plusieurs simulations.

import numpy as np
import matplotlib.pyplot as plt
from simulation import *


def mean_cells(
    type_division: str = "constant",
    type_phase: bool = "constant",
    type_sample: str = "Normal",
    with_all_cells: bool = False,
    n_simuls: int = 500,
) -> "(np.array[float], Cells) or (np.array[float], Cells, list[Cells])":
    """
    Cette fonction permet de calculer la moyenne obtenue pour le nombre de cellules
    au cours du temps obtenus pour les IPPs, IPNs, neurones etc... par notre modèle de 
    neurogenèse. La fonction consiste en d'abord simuler un certains nombre de fois 
    la neurogenèse à l'aide de notre modèle, puis de calculer la moyenne de manière 
    classique.

    Parameters
    ------------
    type_division : str
        Si ce paramètre est à "constant", la neurogenèse se fera avec des temps 
        de cycle constant. Si il est à "gamma", les temps de cycles (ou phases) seront
        aléatoires et suivront une loi gamma dont les paramètres sont préalablement
        remplis dans le fichier parameters.json.

    type_phase : str
        Décide quelle loi va être choisie pour la répartition des phases du cycle
        cellulaire.

    type_sample : str 
        Permet de choisir si nous allons plutôt prendre les paramètres pour la souris
        mutant ou pour la souris normale.

    with_all_cells : bool  
        Paramètre par défaut mis à False qui permet de savoir si la fonction doit 
        retourner ou pas les cellules qui ont permis d'effectuer le calcul de notre 
        espérance.all_cells

    n_simuls : int
        Nombre de simulations que l'on va effectuer afin de calculer notre espérance.
    
    Returns
    ------------
    temps : np.array[float]
        Tableau contenant la liste des temps pour lesquels on a calculé la moyenne
        du nombre de cellules.
    
    mean_cells : Cells
        Objet de type cells comportant les informations sur la moyenne calculée
        pour chaque type de cellules (les informations des arbres de sont pas 
        moyennées donc vides) en fonction du temps. 

    all_cells = list[Cells]
        Retouné seulement si le paramètre "with_all_cells" est mis à True. Il 
        retourne tous les objets cells qui ont permis de calculer la moyenne.
    """

    Neuro = Stochastic_simulator(type_division, type_phase, type_sample)
    len_time = int(
        (
            Neuro.parameters["simulation_parameters"]["time_end"]
            - Neuro.parameters["simulation_parameters"]["time_start"]
        )
        / Neuro.parameters["simulation_parameters"]["step_time"]
    )
    all_IPPs = np.zeros((n_simuls, len_time))
    all_IPNs = np.zeros((n_simuls, len_time))
    all_neurons_deep = np.zeros((n_simuls, len_time))
    all_neurons_up = np.zeros((n_simuls, len_time))

    all_IPP_G1 = np.zeros((n_simuls, len_time))
    all_IPP_S = np.zeros((n_simuls, len_time))
    all_IPP_G2 = np.zeros((n_simuls, len_time))
    all_IPP_M = np.zeros((n_simuls, len_time))

    all_IPN_G1 = np.zeros((n_simuls, len_time))
    all_IPN_S = np.zeros((n_simuls, len_time))
    all_IPN_G2 = np.zeros((n_simuls, len_time))
    all_IPN_M = np.zeros((n_simuls, len_time))

    all_counts_first_injection = np.zeros(n_simuls)
    all_counts_second_injection = np.zeros((n_simuls, len_time))
    all_counts_ratio = np.zeros((n_simuls, len_time))

    all_max_IPP = np.zeros(n_simuls)
    all_max_IPN = np.zeros(n_simuls)

    all_cells = list()
    for simul in range(0, n_simuls):
        temps, cells = Neuro.simulate()
        all_IPPs[simul] = cells.IPP
        all_IPNs[simul] = cells.IPN
        all_neurons_deep[simul] = cells.neurons_deep_layer
        all_neurons_up[simul] = cells.neurons_upper_layer

        all_cells.append(cells)

        all_IPP_G1[simul] = cells.IPP_G1
        all_IPP_S[simul] = cells.IPP_S
        all_IPP_G2[simul] = cells.IPP_G2
        all_IPP_M[simul] = cells.IPP_M

        all_IPN_G1[simul] = cells.IPN_G1
        all_IPN_S[simul] = cells.IPN_S
        all_IPN_G2[simul] = cells.IPN_G2
        all_IPN_M[simul] = cells.IPN_M

        all_counts_first_injection[simul] = cells.counts_first_injection
        all_counts_second_injection[simul] = cells.counts_second_injection
        all_counts_ratio[simul] = cells.ratio_injection

        all_max_IPP[simul] = temps[np.argmax(cells.IPP)]
        all_max_IPN[simul] = temps[np.argmax(cells.IPN)]

    # Calculs des différentes moyennes
    mean_cells = Cells(len_time)
    mean_cells.IPP = np.mean(all_IPPs, axis=0)
    mean_cells.IPN = np.mean(all_IPNs, axis=0)
    mean_cells.neurons_deep_layer = np.mean(all_neurons_deep, axis=0)
    mean_cells.neurons_upper_layer = np.mean(all_neurons_up, axis=0)

    mean_cells.IPP_G1 = np.mean(all_IPP_G1, axis=0)
    mean_cells.IPP_S = np.mean(all_IPP_S, axis=0)
    mean_cells.IPP_G2 = np.mean(all_IPP_G2, axis=0)
    mean_cells.IPP_M = np.mean(all_IPP_M, axis=0)

    mean_cells.IPN_G1 = np.mean(all_IPN_G1, axis=0)
    mean_cells.IPN_S = np.mean(all_IPN_S, axis=0)
    mean_cells.IPN_G2 = np.mean(all_IPN_G2, axis=0)
    mean_cells.IPN_M = np.mean(all_IPN_M, axis=0)

    mean_cells.neurons = mean_cells.neurons_deep_layer + mean_cells.neurons_upper_layer
    mean_cells.IP = mean_cells.IPP + mean_cells.IPN

    mean_cells.counts_first_injection = np.mean(all_counts_first_injection)
    mean_cells.counts_second_injection = np.mean(all_counts_second_injection, axis=0)
    mean_cells.ratio_injection = np.mean(all_counts_ratio, axis=0)

    mean_cells.max_IPP = np.mean(all_max_IPP)
    mean_cells.max_IPN = np.mean(all_max_IPN)

    if with_all_cells:
        return temps, mean_cells, all_cells
    else:
        return temps, mean_cells


def var_cells(
    type_division: str = "constant",
    type_phase: str = "constant",
    type_sample: str = "Normal",
    with_all_cells: bool = False,
    n_simuls: int = 500,
) -> "(np.array[float], Cells) or (np.array[float], Cells, list[Cells])":
    """
    Cette fonction permet de calculer la variance obtenue pour le nombre de cellules
    au cours du temps obtenus pour les IPPs, IPNs, neurones etc... par notre modèle de 
    neurogenèse. La fonction consiste en d'abord simuler un certains nombre de fois 
    la neurogenèse à l'aide de notre modèle, puis de calculer la variance de manière 
    classique.

    Parameters
    ------------
    type_division : str
        Si ce paramètre est à "constant", la neurogenèse se fera avec des temps 
        de cycle constant. Si il est à "gamma", les temps de cycles (ou phases) seront
        aléatoires et suivront une loi gamma dont les paramètres sont préalablement
        remplis dans le fichier parameters.json.

    type_phase : str
        Décide quelle loi va être choisie pour la répartition des phases du cycle
        cellulaire.

    type_sample : str 
        Permet de choisir si nous allons plutôt prendre les paramètres pour la souris
        mutant ou pour la souris normale.

    with_all_cells : bool  
        Paramètre par défaut mis à False qui permet de savoir si la fonction doit 
        retourner ou pas les cellules qui ont permis d'effectuer le calcul de notre 
        espérance.all_cells

    n_simuls : int
        Nombre de simulations que l'on va effectuer afin de calculer notre espérance.

    Returns
    ------------
    temps : np.array[float]
        Tableau contenant la liste des temps pour lesquels on a calculé la variance
        du nombre de cellules.
    
    var_cells : Cells
        Objet de type cells comportant les informations sur la variance calculée
        pour chaque type de cellules (les informations des arbres de sont pas 
        moyennées donc vides) en fonction du temps. 

    all_cells = list[Cells]
        Retouné seulement si le paramètre "with_all_cells" est mis à True. Il 
        retourne tous les objets cells qui ont permis de calculer la variance.
    """

    Neuro = Stochastic_simulator(type_division, type_phase, type_sample)
    len_time = int(
        (
            Neuro.parameters["simulation_parameters"]["time_end"]
            - Neuro.parameters["simulation_parameters"]["time_start"]
        )
        / Neuro.parameters["simulation_parameters"]["step_time"]
    )  # -1 car pour rappel lors de la simulation, on enlève la dernière case du tableau.

    all_IPPs = np.zeros((n_simuls, len_time))
    all_IPNs = np.zeros((n_simuls, len_time))
    all_neurons_deep = np.zeros((n_simuls, len_time))
    all_neurons_up = np.zeros((n_simuls, len_time))

    all_IPP_G1 = np.zeros((n_simuls, len_time))
    all_IPP_S = np.zeros((n_simuls, len_time))
    all_IPP_G2 = np.zeros((n_simuls, len_time))
    all_IPP_M = np.zeros((n_simuls, len_time))

    all_IPN_G1 = np.zeros((n_simuls, len_time))
    all_IPN_S = np.zeros((n_simuls, len_time))
    all_IPN_G2 = np.zeros((n_simuls, len_time))
    all_IPN_M = np.zeros((n_simuls, len_time))

    all_counts_first_injection = np.zeros(n_simuls)
    all_counts_second_injection = np.zeros((n_simuls, len_time))
    all_counts_ratio = np.zeros((n_simuls, len_time))

    all_max_IPP = np.zeros(n_simuls)
    all_max_IPN = np.zeros(n_simuls)

    all_cells = list()
    for simul in range(0, n_simuls):
        temps, cells = Neuro.simulate()
        all_IPPs[simul] = cells.IPP
        all_IPNs[simul] = cells.IPN
        all_neurons_deep[simul] = cells.neurons_deep_layer
        all_neurons_up[simul] = cells.neurons_upper_layer
        all_cells.append(cells)

        all_IPP_G1[simul] = cells.IPP_G1
        all_IPP_S[simul] = cells.IPP_S
        all_IPP_G2[simul] = cells.IPP_G2
        all_IPP_M[simul] = cells.IPP_M

        all_IPN_G1[simul] = cells.IPN_G1
        all_IPN_S[simul] = cells.IPN_S
        all_IPN_G2[simul] = cells.IPN_G2
        all_IPN_M[simul] = cells.IPN_M

        all_counts_first_injection[simul] = cells.counts_first_injection
        all_counts_second_injection[simul] = cells.counts_second_injection
        all_counts_ratio[simul] = cells.ratio_injection

        all_max_IPP[simul] = temps[np.argmax(cells.IPP)]
        all_max_IPN[simul] = temps[np.argmax(cells.IPN)]

    # Calculs des différentes moyennes
    var_cells = Cells(len_time)
    var_cells.IPP = np.var(all_IPPs, axis=0)
    var_cells.IPN = np.var(all_IPNs, axis=0)
    var_cells.neurons_deep_layer = np.var(all_neurons_deep, axis=0)
    var_cells.neurons_upper_layer = np.var(all_neurons_up, axis=0)
    var_cells.neurons = np.var(all_neurons_up + all_neurons_deep, axis=0)
    var_cells.IP = np.var(all_IPPs + all_IPNs, axis=0)

    var_cells.IPP_G1 = np.var(all_IPP_G1, axis=0)
    var_cells.IPP_S = np.var(all_IPP_S, axis=0)
    var_cells.IPP_G2 = np.var(all_IPP_G2, axis=0)
    var_cells.IPP_M = np.var(all_IPP_M, axis=0)

    var_cells.IPN_G1 = np.var(all_IPN_G1, axis=0)
    var_cells.IPN_S = np.var(all_IPN_S, axis=0)
    var_cells.IPN_G2 = np.var(all_IPN_G2, axis=0)
    var_cells.IPN_M = np.var(all_IPN_M, axis=0)

    var_cells.counts_first_injection = np.var(all_counts_first_injection)
    var_cells.counts_second_injection = np.var(all_counts_second_injection, axis=0)
    var_cells.ratio_injection = np.var(all_counts_ratio, axis=0)

    var_cells.max_IPP = np.var(all_max_IPP)
    var_cells.max_IPN = np.var(all_max_IPN)

    if with_all_cells:
        return temps, var_cells, all_cells
    else:
        return temps, var_cells
