# Fonctions utiles liées à la fonction sigmoide.

import numpy as np
import matplotlib.pyplot as plt


def sigmoide(
    time: float,
    param_1_slope: float,
    param_2_location: float,
    param_3_plus_inf: float = 0,
    param_4_moins_inf: float = 1,
) -> float:

    """
    Cette fonction est la fonction utilisée en général pour les fonctions liées 
    aux taux de division. Il s'agit d'une fonction sous la forme "sigmoide".

    Parameters
    ------------
    time : float
        Point où nous allons calculer la valeur de la fonction.

    param_1_slope : float
        Influe (de manière croissante en valeur absolue) sur la valeur de la 
        dérivée au point d'inflexion de cette fonction sigmoïde.

    param_2_location : float
        Endroit où se situe le point d'inflexion.

    param_3_plus_inf : float
        Valeur de la fonction en +infini.

    param_4_moins_inf : float
        Valeur de la fonction en -infini.
    """

    return param_3_plus_inf + (param_4_moins_inf - param_3_plus_inf) / (
        1 + np.exp(param_1_slope * (time - param_2_location))
    )


def plot_sigmoide(params: "np.array"):
    """
    Cette fonction peremet de tracer la fonction sigmoide pour ds paramètres 
    donnés.

    Parameters
    ------------
    params : "np.array"
        Paramètres de la fonction simoide.
    """

    time_start = -1
    time_end = 30
    n_points = 1000
    times = np.linspace(time_start, time_end, n_points)
    values_sigmoide = np.zeros(n_points)

    for k in range(0, n_points):

        values_sigmoide[k] = sigmoide(
            times[k], params[0], params[1], params[2], params[3]
        )
    fig = plt.figure()
    plt.plot(times, values_sigmoide)
    plt.xlabel("Temps")
    plt.ylabel("Sigmoide")
    plt.title("Fonction sigmoide pour les paramètres demandés")
    plt.show()


def derivee_first_var_sig(
    time: float,
    param_1_slope: float,
    param_2_location: float,
    param_3_plus_inf: float = 0,
    param_4_moins_inf: float = 1,
) -> "function":
    """
    Cette fonction renvoie une fonction permettant de calculer la dérivée par 
    rapport au premier paramètre de la fonction sigmoide shiftée.

    Parameters
    ------------
    time : float
        Temps où l'évènement se produit, correspond également au temps que l'on va
        utiliser pour le shift/
        
    param_1_slope : float
        Influe (de manière croissante en valeur absolue) sur la valeur de la 
        dérivée au point d'inflexion de cette fonction sigmoïde.

    param_2_location : float
        Endroit où se situe le point d'inflexion.

    param_3_plus_inf : float
        Valeur de la fonction en +infini.

    param_4_moins_inf : float
        Valeur de la fonction en -infini.
    """

    def shift_derivee_first(
        time_division: float, time_second_division: float = 0
    ) -> float:
        """
        Fonction qui renvoie la valeur de la dérivée par rapport au premier paramètre 
        de la fonction sigmoide shiftée. Cette fonction peut avoir deux variables,
        car chacune de ces variables correspondent aux instant de division des IPP/IPN
        et que les IPN se divisent deux fois. 

        Parameters
        ------------
        time_division : float
            Instant de la première division après que l'évènement se soit produit (création IPP ou IPN).

        time_second_division : float
            Instant de la seconde division après que l'évènement se soit produit (création IPP ou IPN).
        """

        return (
            (time + time_division + time_second_division - param_2_location)
            * (param_3_plus_inf - param_4_moins_inf)
            * np.exp(
                param_1_slope
                * (time + time_division + time_second_division - param_2_location)
            )
            / (
                (
                    1
                    + np.exp(
                        param_1_slope
                        * (
                            time
                            + time_division
                            + time_second_division
                            - param_2_location
                        )
                    )
                )
                ** 2
            )
        )

    return shift_derivee_first


def derivee_second_var_sig(
    time: float,
    param_1_slope: float,
    param_2_location: float,
    param_3_plus_inf: float = 0,
    param_4_moins_inf: float = 1,
) -> "function":
    """
    Cette fonction renvoie une fonction permettant de calculer la dérivée par 
    rapport au second paramètre de la fonction sigmoide shiftée.

    Parameters
    ------------
    time : float
        Temps où l'évènement se produit, correspond également au temps que l'on va
        utiliser pour le shift.
        
    param_1_slope : float
        Influe (de manière croissante en valeur absolue) sur la valeur de la 
        dérivée au point d'inflexion de cette fonction sigmoïde.

    param_2_location : float
        Endroit où se situe le point d'inflexion.

    param_3_plus_inf : float
        Valeur de la fonction en +infini.

    param_4_moins_inf : float
        Valeur de la fonction en -infini.
    """

    def shift_derivee_second(
        time_division: float, time_second_division: float = 0
    ) -> float:
        """
        Fonction qui renvoie la valeur de la dérivée par rapport au second paramètre 
        de la fonction sigmoide shiftée. Cette fonction peut avoir deux variables,
        car chacune de ces variables correspondent aux instant de division des IPP/IPN
        et que les IPN se divisent deux fois. 

        Parameters
        ------------
        time_division : float
            Instant de la première division après que l'évènement se soit produit (création IPP ou IPN).

        time_second_division : float
            Instant de la seconde division après que l'évènement se soit produit (création IPP ou IPN).
        """
        return (
            param_1_slope
            * (param_4_moins_inf - param_3_plus_inf)
            * np.exp(
                param_1_slope
                * (time + time_division + time_second_division - param_2_location)
            )
            / (
                (
                    1
                    + np.exp(
                        param_1_slope
                        * (
                            time
                            + time_division
                            + time_second_division
                            - param_2_location
                        )
                    )
                )
                ** 2
            )
        )

    return shift_derivee_second


def derivee_troisieme_var_sig(
    time: float, param_1_slope: float, param_2_location: float, negative: bool = False
) -> "function":
    """
    Cette fonction renvoie une fonction permettant de calculer la dérivée par 
    rapport au troisième paramètre de la fonction sigmoide shiftée. Elle contient 
    un paramètre permettant d'obtenir cette dérivée dans le cas négatif très simplement
    (utile pour calculer la dérivée de 1-Delta par exemple).

    Parameters
    ------------
    time : float
        Temps où l'évènement se produit, correspond également au temps que l'on va
        utiliser pour le shift.
        
    param_1_slope : float
        Influe (de manière croissante en valeur absolue) sur la valeur de la 
        dérivée au point d'inflexion de cette fonction sigmoïde.

    param_2_location : float
        Endroit où se situe le point d'inflexion.

    param_3_plus_inf : float
        Valeur de la fonction en +infini.

    param_4_moins_inf : float
        Valeur de la fonction en -infini.
    """

    def shift_derivee_third(
        time_division: float, time_second_division: float = 0
    ) -> float:
        """
        Fonction qui renvoie la valeur de la dérivée par rapport au troisième paramètre 
        de la fonction sigmoide shiftée. Cette fonction peut avoir deux variables,
        car chacune de ces variables correspondent aux instant de division des IPP/IPN
        et que les IPN se divisent deux fois. 

        Parameters
        ------------
        time_division : float
            Instant de la première division après que l'évènement se soit produit (création IPP ou IPN).

        time_second_division : float
            Instant de la seconde division après que l'évènement se soit produit (création IPP ou IPN).
        """

        if negative:
            value_plus_inf = -1
            value_moins_inf = 0
        else:
            value_plus_inf = 1
            value_moins_inf = 0
        return sigmoide(
            time + time_division + time_second_division,
            param_1_slope,
            param_2_location,
            value_plus_inf,
            value_moins_inf,
        )

    return shift_derivee_third


def derivee_quatrieme_var_sig(
    time: float, param_1_slope: float, param_2_location: float, negative: bool = False
) -> "function":
    """
    Cette fonction renvoie une fonction permettant de calculer la dérivée par 
    rapport au quatrième paramètre de la fonction sigmoide shiftée. Elle contient 
    un paramètre permettant d'obtenir cette dérivée dans le cas négatif très simplement
    (utile pour calculer la dérivée de 1-Delta par exemple).

    Parameters
    ------------
    time : float
        Temps où l'évènement se produit, correspond également au temps que l'on va
        utiliser pour le shift.
        
    param_1_slope : float
        Influe (de manière croissante en valeur absolue) sur la valeur de la 
        dérivée au point d'inflexion de cette fonction sigmoïde.

    param_2_location : float
        Endroit où se situe le point d'inflexion.

    param_3_plus_inf : float
        Valeur de la fonction en +infini.

    param_4_moins_inf : float
        Valeur de la fonction en -infini.
    """

    def shift_derivee_fourth(
        time_division: float, time_second_division: float = 0
    ) -> float:
        """
        Fonction qui renvoie la valeur de la dérivée par rapport au quatrième paramètre 
        de la fonction sigmoide shiftée. Cette fonction peut avoir deux variables,
        car chacune de ces variables correspondent aux instant de division des IPP/IPN
        et que les IPN se divisent deux fois. 

        Parameters
        ------------
        time_division : float
            Instant de la première division après que l'évènement se soit produit (création IPP ou IPN).

        time_second_division : float
            Instant de la seconde division après que l'évènement se soit produit (création IPP ou IPN).
        """
        if negative:
            value_plus_inf = 0
            value_moins_inf = -1
        else:
            value_plus_inf = 0
            value_moins_inf = 1
        return sigmoide(
            time + time_division + time_second_division,
            param_1_slope,
            param_2_location,
            value_plus_inf,
            value_moins_inf,
        )

    return shift_derivee_fourth


def derivee_first_var_squared(
    time: float,
    param_1_slope: float,
    param_2_location: float,
    param_3_plus_inf: float = 0,
    param_4_moins_inf: float = 1,
) -> "function":
    """
    Cette fonction renvoie une fonction permettant de calculer la dérivée par 
    rapport au premier paramètre de la fonction sigmoide carré shiftée.

    Parameters
    ------------
    time : float
        Temps où l'évènement se produit, correspond également au temps que l'on va
        utiliser pour le shift/
        
    param_1_slope : float
        Influe (de manière croissante en valeur absolue) sur la valeur de la 
        dérivée au point d'inflexion de cette fonction sigmoïde.

    param_2_location : float
        Endroit où se situe le point d'inflexion.

    param_3_plus_inf : float
        Valeur de la fonction en +infini.

    param_4_moins_inf : float
        Valeur de la fonction en -infini.
    """

    def shift_derivee_first(
        time_division: float, time_second_division: float = 0
    ) -> float:
        """
        Fonction qui renvoie la valeur de la dérivée par rapport au premier paramètre 
        de la fonction sigmoide carré shiftée. Cette fonction peut avoir deux variables,
        car chacune de ces variables correspondent aux instant de division des IPP/IPN
        et que les IPN se divisent deux fois. 

        Parameters
        ------------
        time_division : float
            Instant de la première division après que l'évènement se soit produit (création IPP ou IPN).

        time_second_division : float
            Instant de la seconde division après que l'évènement se soit produit (création IPP ou IPN).
        """
        return (
            2
            * (
                (time + time_division + time_second_division - param_2_location)
                * (param_3_plus_inf - param_4_moins_inf)
                * np.exp(
                    param_1_slope
                    * (time + time_division + time_second_division - param_2_location)
                )
                / (
                    (
                        1
                        + np.exp(
                            param_1_slope
                            * (
                                time
                                + time_division
                                + time_second_division
                                - param_2_location
                            )
                        )
                    )
                    ** 2
                )
            )
            * sigmoide(
                time + time_division + time_second_division,
                param_1_slope,
                param_2_location,
                param_3_plus_inf,
                param_4_moins_inf,
            )
        )

    return shift_derivee_first


def derivee_second_var_squared(
    time: float,
    param_1_slope: float,
    param_2_location: float,
    param_3_plus_inf: float = 0,
    param_4_moins_inf: float = 1,
) -> "function":
    """
    Cette fonction renvoie une fonction permettant de calculer la dérivée par 
    rapport au second paramètre de la fonction sigmoide carré shiftée.

    Parameters
    ------------
    time : float
        Temps où l'évènement se produit, correspond également au temps que l'on va
        utiliser pour le shift.
        
    param_1_slope : float
        Influe (de manière croissante en valeur absolue) sur la valeur de la 
        dérivée au point d'inflexion de cette fonction sigmoïde.

    param_2_location : float
        Endroit où se situe le point d'inflexion.

    param_3_plus_inf : float
        Valeur de la fonction en +infini.

    param_4_moins_inf : float
        Valeur de la fonction en -infini.
    """

    def shift_derivee_second(
        time_division: float, time_second_division: float = 0
    ) -> float:
        """
        Fonction qui renvoie la valeur de la dérivée par rapport au second paramètre 
        de la fonction sigmoide carré shiftée. Cette fonction peut avoir deux variables,
        car chacune de ces variables correspondent aux instant de division des IPP/IPN
        et que les IPN se divisent deux fois. 

        Parameters
        ------------
        time_division : float
            Instant de la première division après que l'évènement se soit produit (création IPP ou IPN).

        time_second_division : float
            Instant de la seconde division après que l'évènement se soit produit (création IPP ou IPN).
        """
        return (
            2
            * (
                param_1_slope
                * (param_4_moins_inf - param_3_plus_inf)
                * np.exp(
                    param_1_slope
                    * (time + time_division + time_second_division - param_2_location)
                )
                / (
                    (
                        1
                        + np.exp(
                            param_1_slope
                            * (
                                time
                                + time_division
                                + time_second_division
                                - param_2_location
                            )
                        )
                    )
                    ** 2
                )
            )
            * sigmoide(
                time + time_division + time_second_division,
                param_1_slope,
                param_2_location,
                param_3_plus_inf,
                param_4_moins_inf,
            )
        )

    return shift_derivee_second


def derivee_troisieme_var_squared(
    time: float,
    param_1_slope: float,
    param_2_location: float,
    param_3_plus_inf: float = 0,
    param_4_moins_inf: float = 1,
    negative: bool = False,
) -> float:
    """
    Cette fonction renvoie une fonction permettant de calculer la dérivée par 
    rapport au troisième paramètre de la fonction sigmoide carré shiftée. Elle contient 
    un paramètre permettant d'obtenir cette dérivée dans le cas négatif très simplement
    (utile pour calculer la dérivée de 1-Delta par exemple).

    Parameters
    ------------
    time : float
        Temps où l'évènement se produit, correspond également au temps que l'on va
        utiliser pour le shift.
        
    param_1_slope : float
        Influe (de manière croissante en valeur absolue) sur la valeur de la 
        dérivée au point d'inflexion de cette fonction sigmoïde.

    param_2_location : float
        Endroit où se situe le point d'inflexion.

    param_3_plus_inf : float
        Valeur de la fonction en +infini.

    param_4_moins_inf : float
        Valeur de la fonction en -infini.
    """

    def shift_derivee_third(
        time_division: float, time_second_division: float = 0
    ) -> float:
        """
        Fonction qui renvoie la valeur de la dérivée par rapport au troisième paramètre 
        de la fonction sigmoide carré shiftée. Cette fonction peut avoir deux variables,
        car chacune de ces variables correspondent aux instant de division des IPP/IPN
        et que les IPN se divisent deux fois. 

        Parameters
        ------------
        time_division : float
            Instant de la première division après que l'évènement se soit produit (création IPP ou IPN).

        time_second_division : float
            Instant de la seconde division après que l'évènement se soit produit (création IPP ou IPN).
        """
        if negative:
            value_plus_inf = -1
            value_moins_inf = 0
            return (
                2
                * sigmoide(
                    time + time_division + time_second_division,
                    param_1_slope,
                    param_2_location,
                    value_plus_inf,
                    value_moins_inf,
                )
                * (
                    1
                    - sigmoide(
                        time + time_division + time_second_division,
                        param_1_slope,
                        param_2_location,
                        param_3_plus_inf,
                        param_4_moins_inf,
                    )
                )
            )
        else:
            value_plus_inf = 1
            value_moins_inf = 0
            return (
                2
                * sigmoide(
                    time + time_division + time_second_division,
                    param_1_slope,
                    param_2_location,
                    value_plus_inf,
                    value_moins_inf,
                )
                * sigmoide(
                    time + time_division + time_second_division,
                    param_1_slope,
                    param_2_location,
                    param_3_plus_inf,
                    param_4_moins_inf,
                )
            )

    return shift_derivee_third


def derivee_quatrieme_var_squared(
    time: float,
    param_1_slope: float,
    param_2_location: float,
    param_3_plus_inf: float = 0,
    param_4_moins_inf: float = 1,
    negative: bool = False,
) -> "function":
    """
    Cette fonction renvoie une fonction permettant de calculer la dérivée par 
    rapport au quatrième paramètre de la fonction sigmoide shiftée. Elle contient 
    un paramètre permettant d'obtenir cette dérivée dans le cas négatif très simplement
    (utile pour calculer la dérivée de 1-Delta par exemple).

    Parameters
    ------------
    time : float
        Temps où l'évènement se produit, correspond également au temps que l'on va
        utiliser pour le shift.
        
    param_1_slope : float
        Influe (de manière croissante en valeur absolue) sur la valeur de la 
        dérivée au point d'inflexion de cette fonction sigmoïde.

    param_2_location : float
        Endroit où se situe le point d'inflexion.

    param_3_plus_inf : float
        Valeur de la fonction en +infini.

    param_4_moins_inf : float
        Valeur de la fonction en -infini.
    """

    def shift_derivee_fourth(
        time_division: float, time_second_division: float = 0
    ) -> float:
        """
        Fonction qui renvoie la valeur de la dérivée par rapport au quatrième paramètre 
        de la fonction sigmoide carré shiftée. Cette fonction peut avoir deux variables,
        car chacune de ces variables correspondent aux instant de division des IPP/IPN
        et que les IPN se divisent deux fois. 

        Parameters
        ------------
        time_division : float
            Instant de la première division après que l'évènement se soit produit (création IPP ou IPN).

        time_second_division : float
            Instant de la seconde division après que l'évènement se soit produit (création IPP ou IPN).
        """
        if negative:
            value_plus_inf = 0
            value_moins_inf = -1
            return (
                2
                * sigmoide(
                    time + time_division + time_second_division,
                    param_1_slope,
                    param_2_location,
                    value_plus_inf,
                    value_moins_inf,
                )
                * (
                    1
                    - sigmoide(
                        time + time_division + time_second_division,
                        param_1_slope,
                        param_2_location,
                        param_3_plus_inf,
                        param_4_moins_inf,
                    )
                )
            )
        else:
            value_plus_inf = 0
            value_moins_inf = 1
            return (
                2
                * sigmoide(
                    time + time_division + time_second_division,
                    param_1_slope,
                    param_2_location,
                    value_plus_inf,
                    value_moins_inf,
                )
                * sigmoide(
                    time + time_division + time_second_division,
                    param_1_slope,
                    param_2_location,
                    param_3_plus_inf,
                    param_4_moins_inf,
                )
            )

    return shift_derivee_fourth

