# Toutes les petites fonctions utiles pour simuler la neurogenèse ou récupérer
# les données expérimentales.
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import scipy.stats


def compute_index_time(time_to_index: float, times: "np.array[float]") -> int:
    """
    Cette fonction permet de calculer pour un temps et un tableau (contenant des
    temps en ordre croissant) donnés à quel indice du tableau correspond ce temps 
    (c'est-à-dire l'indice correspondant au dernier temps inférieur au temps où 
    on cherche l'indice).

    Exemples
    ------------

    >>> compute_index_time(time_to_index = 6, times = np.array([1,2,5,7,9]))
        2

    >>> compute_index_time(time_to_index = 3, times = np.array([1,3,4,5,15]))
        1

    Parameters
    ------------
    time_to_index : float
        Temps que l'on cherche à indexer.

    times : "np.array[float]"
        Tableau avec les temps ordonnés dans lequel on va chercher l'indice du 
        temps qu'on cherche à indexer.
    """
    return np.where(times - time_to_index <= 0)[0][-1]


def remove_last_index(array: "np.array") -> "np.array":
    """
    Cette fonction permet d'enlever le dernier élément d'un tableau.

    Parameters
    ------------
    array : "np.array"
        Tableau dont on veut enlever le dernier élément.
    """

    return np.delete(
        array, -1
    )  # -1 correspond à l'indice du dernier élément du tableau.


def sigmoide(
    time: float,
    param_1_slope: float,
    param_2_location: float,
    param_3_plus_inf: float = 0,
    param_4_moins_inf: float = 1,
) -> float:

    """
    Cette fonction est la fonction utilisée en général pour les fonctions liées 
    aux taux de division. Il s'agit d'une fonction sous la forme "sigmoide".

    Parameters
    ------------
    time : float
        Point où nous allons calculer la valeur de la fonction.

    param_1_slope : float
        Influe (de manière croissante en valeur absolue) sur la valeur de la 
        dérivée au point d'inflexion de cette fonction sigmoïde.

    param_2_location : float
        Endroit où se situe le point d'inflexion.

    param_3_plus_inf : float
        Valeur de la fonction en +infini.

    param_4_moins_inf : float
        Valeur de la fonction en -infini.
    """

    return param_3_plus_inf + (param_4_moins_inf - param_3_plus_inf) / (
        1 + np.exp(param_1_slope * (time - param_2_location))
    )


def gaussian(point: float, param_1_moyenne: float, param_2_variance: float) -> float:
    """
    Cette fonction retourne la valeur de la densité d'une gaussienne à un point donné 
    en fonction du temps.

    Parameters
    ------------
    point : float
        Point où l'on veut calculer la densité de la gaussienne.

    param_1_moyenne : float
        Moyenne de la gaussienne dont on veut la densité.
    
    param_2_variance : float
        Variance de la gaussienne dont on veut la densité.
    """

    return (1 / np.sqrt(2 * np.pi * param_2_variance)) * (
        np.exp(-((point - param_1_moyenne) ** 2 / (2 * param_2_variance)))
    )


def gamma(param_1_moyenne: float, param_2_variance: float) -> float:
    """
    Cette fonction retourne la valeur d'une réalisation d'une variable aléatoire 
    qui suit une loi gamma d'une certaine moyenne et d'une certaine variance. 

    Parameters
    ------------
    param_1_moyenne : float
        Moyenne de la loi gamma dont on veut obtenir une réalisation.
    
    param_2_variance : float
        Variance de la loi gamma dont on veut obtenir une réalisation.
    """

    param_1_gamma = (param_1_moyenne ** 2) / param_2_variance
    param_2_gamma = param_2_variance / param_1_moyenne

    return scipy.stats.gamma.rvs(param_1_gamma, scale=param_2_gamma)


def exponential(param_exp: float,) -> float:
    """
    Cette fonction retourne la valeur d'une réalisation d'une variable aléatoire 
    qui suit une loi exponentielle d'un certain paramètre. 

    Parameters
    ------------
    param_exp: float
        Paramètre de la loi exponentielle dont on veut obtenir une réalisation.
    """

    return scipy.stats.expon.rvs(scale=1 / param_exp)


def symetric_beta(param_1_moyenne: float, param_2_variance: float) -> float:
    """
    Cette fonction retourne la valeur d'une réalisation d'une variable aléatoire 
    qui suit une loi beta symétrique d'une certaine moyenne et d'une certaine variance. 

    Parameters
    ------------
    param_1_moyenne : float
        Moyenne de la loi beta dont on veut obtenir une réalisation.
    
    param_2_variance : float
        Variance de la loi beta dont on veut obtenir une réalisation.
    """
    param_1_beta = (param_1_moyenne ** 2 - param_2_variance) / (2 * param_2_variance)
    param_2_beta = (param_1_moyenne ** 2 - param_2_variance) / (2 * param_2_variance)

    return 2 * scipy.stats.beta.rvs(param_1_beta, param_2_beta) * param_1_moyenne


def beta_phase(
    percent_phase: list, var_S_phase: float, ecart_max_to_mean: float = 1 / 20
) -> "np.array":
    """
    Cette fonction permet de tirer des proportions de phases, lorsque les seules
    qui sont aléatoires sont la phase S et la phase G1, et que la phase S s'obtient
    à l'aide d'une loi beta renormalisée puis shiftée.

    Parameters
    ------------
    percent_phase : list
        Valeur moyenne de la proportion de chaque phase dans le cycle total.
    
    var_S_phase : float
        Variance de la phase S pour cette loi.

    ecart_max_to_mean
        Valeur maximale de l'écart de la phase S avec sa moyenne.
    """

    # Léger traitement des données
    percent_phase = np.array(percent_phase)  # List to array
    index_phase_G1 = 0
    index_phase_S = 1

    # Calcul des paramètres de la loi beta
    param_1_beta = (ecart_max_to_mean ** 2 - var_S_phase) / (2 * var_S_phase)
    param_2_beta = (ecart_max_to_mean ** 2 - var_S_phase) / (2 * var_S_phase)

    # Calcul de la proportion de la durée des phases G1/S dans la durée totale du cycle
    new_phase_S = percent_phase[index_phase_S] + (
        (2 * scipy.stats.beta.rvs(param_1_beta, param_2_beta) - 1) * ecart_max_to_mean
    )
    new_phase_G1 = percent_phase[index_phase_G1] + (
        percent_phase[index_phase_S] - new_phase_S
    )

    # Mise à jour
    percent_phase[index_phase_S] = new_phase_S
    percent_phase[index_phase_G1] = new_phase_G1

    return percent_phase


def multinomial_phase(
    percent_phase: list, param_n_event_multi: int = 200, shift: float = 1
) -> "np.array":
    """
    Cette fonction permet de tirer des proportions de phases qui suivent une loi 
    multinomiale shiftée.

    Parameters
    ------------
    percent_phase : list
        Valeur moyenne de la proportion de chaque phase dans le cycle total.
    
    param_n_event_mutli : int
        Nombre d'évènements pour notre loi multinomiale.
    
    shift : float 
        Valeur du shift que nous allons utiliser.
    """

    # afin d'éviter d'avoir des valeurs à 0 pour la durée des cycles
    n_param = 4
    return (scipy.stats.multinomial.rvs(param_n_event_multi, percent_phase) + shift) / (
        param_n_event_multi + n_param * shift
    )


def mean_var_cells(
    data: pd.DataFrame, type_cell: str, type_sample: str = "Normal"
) -> ("np.array", "np.array", "np.array"):
    """
    Cette fonction permet après que le fichier correspondant aux données expérimentales
    ait été importé en tant que dataframe de pouvoir récupérer pour un type de cellule
    donné la moyenne et la variance du nombre de cellules pour chaque temps lié
    à l'expérience.

    Parameters
    ------------
    data : pd.DataFrame
        Dataframe comportant les données expérimentales.
    
    type_cell : str
        Nom du type de cellule dont on veut obtenir la moyenne et la variance pour chaque
        temps.
    
    type_sample : str
        Permet de savoir si nous allons récupérer ces informations pour les souris 
        normales ou les souris mutantes.
    """

    data = data[data["Samples"] == type_sample]

    times = data["Times"].values
    n_cells = data[type_cell].values

    unique_times = np.unique(times)

    if (type_cell == "N") or (type_cell == "UL") or (type_cell == "DL"):
        min_time_neurons = 12.5
        max_time_neurons = (
            16.5  # Les données sont utilisables uniquement dans l'intervalle 13.5 16.5
        )

        index_min = np.where(unique_times == min_time_neurons)[0][0]
        index_max = np.where(unique_times > max_time_neurons)[0][
            0
        ]  # Premier indice supérieur
        unique_times = unique_times[index_min:index_max]

    means = np.zeros(
        len(unique_times)
    )  # Tableau qui contient les moyennes que nous allons calculer pour chaque temps différent
    variances = np.zeros(
        len(unique_times)
    )  # Tableau qui contient les variances que nous allons calculer pour chaque temps différent

    for k in range(0, len(unique_times)):

        means[k] = np.mean(n_cells[np.where(times == unique_times[k])])
        variances[k] = np.var(n_cells[np.where(times == unique_times[k])])

    return unique_times, means, variances

