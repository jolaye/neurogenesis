# Fonction utiles afin de tracer le résultat de d'optimisation.

import numpy as np
import matplotlib.pyplot as plt
from optimization.optimizers import *
from optimization.functions_to_opt import *


def opt_and_plot(
    function_optimization: str, type_optimizer: str, param_init: "np.array" = None,
):
    """
    Cette fonction lance l'optimisation d'une fonction voulue et une méthode voulue,
    puis trace différentes figures afin de montrer les résultats de celle-ci. 
    Notamment, la fonction va tracer l'évolution du critère d'optimisation en 
    fonction des itérations, l'évolution de chacun des paramètres en fonction des 
    itérations, ainsi que la fonction finale trouvée. 

    Parameters
    ------------
    function_optimisation : str
        Nom de la fonction que nous voulons optimiser.

    type_optimizer : str
        Type d'optimisation que nous allons effectuer.

    param_init : "np.array"
        Paramètres initiaux pour notre optimisation (ne sera pas pris en compte 
        lorsque la méthode d'optimisation ne nécessite pas de paramètres initiaux
        comme l'algorithme génétique).
    """
    # En fonction du paramètre on choisit la fonction à optimiser
    if function_optimization == "layers":
        func_to_opt = Params_layers()

    # En fonction du paramètre on choisit l'optimizer
    if type_optimizer == "genetic":
        optimizer = Genetic_optimizer(func_to_opt)
        params, vals = optimizer.optimize()
        vals = np.array(vals)
        vals = np.abs(vals)

    if type_optimizer == "gradient":
        optimizer = Gradient_optimizer(func_to_opt)
        params, vals = optimizer.optimize(param_init)
        vals = np.array(vals)
        vals = np.abs(vals)

    start_it = 1
    iterations = np.arange(start_it, len(params) + start_it)
    ## Évolution de la valeur du critère au fil des itérations
    coeff_ylim_min = 0.9
    coeff_ylim_max = 1.1
    fig = plt.figure()
    plt.plot(iterations, vals)
    plt.xlabel("Nombre d'itérations")
    plt.ylabel("Valeur du critère d'optimisation")
    plt.ylim((coeff_ylim_min * np.min(vals), coeff_ylim_max * np.max(vals)))
    plt.title("Évolution du critère d'optimisation au fil des itérations")
    plt.show()

    ## Évolution des différents paramètres au fil des itérations
    n_plot = len(func_to_opt.params_name)
    n_col = 2
    size_fig = 12

    fig.suptitle("Évolution des différents paramètres au fil des itérations")
    fig, axs = plt.subplots(
        (n_plot - 1) // n_col + 1, n_col, figsize=(size_fig, size_fig)
    )

    for index_param in range(0, n_plot):

        if n_plot > n_col:
            axs[index_param // n_col, index_param % n_col].plot(
                iterations, params[:, index_param]
            )
            axs[index_param // n_col, index_param % n_col].set_title(
                "Paramètre {}".format(func_to_opt.params_name[index_param])
            )
            axs[index_param // n_col, index_param % n_col].set_ylim(
                func_to_opt.params_min[index_param], func_to_opt.params_max[index_param]
            )
        else:
            axs[index_param % n_col].plot(iterations, params[:, index_param])
            axs[index_param % n_col].set_title(
                "Paramètre {}".format(func_to_opt.params_name[index_param])
            )
            axs[index_param % n_col].set_ylim(
                func_to_opt.params_min[index_param], func_to_opt.params_max[index_param]
            )

    for ax in axs.flat:
        ax.set(xlabel="Nombre d'itérations", ylabel="Valeurs paramètres")

    plt.show()

