# Fonctions utiles afin d'afficher les résultats de nos simulations

import numpy as np
import matplotlib.pyplot as plt
from utilities.functions.moments import mean_cells, var_cells
from simulation import Cells, Deterministic_simulator


def plot_cells(
    temps: "np.array[float]", cells: Cells, by_phase: bool = False,
):
    """
    Cette fonction permet de tracer le nombre de IPP en fonction du temps,
    le nombre de IPN en fonction du temps ainsi que le nombre de neurones (DL et 
    UL aussi) en fonction du temps à l'aide d'un objet de type Cells comportant 
    ces informations.

    Parameters
    ------------
    temps : np.array[float]
        Tableau comportant la liste des temps dont on veut connaître le nombre de 
        chaque type de cellule.

    cells : Cells
        Objet de type Cells comportant toutes les informations voulues concernant
        le nombre de chaque type de cellules en fonction du temps.

    by_phase: bool 
        Si ce paramètre est sur vrai, nous allons également afficher les différentes
        phases.
    """

    fig = plt.figure()
    plt.plot(temps, cells.IPP)
    plt.xlabel("Temps")
    plt.ylabel("Nombre de cellules")
    plt.title("IPP en fonction du temps")
    plt.show()

    fig = plt.figure()
    plt.plot(temps, cells.IPN)
    plt.xlabel("Temps")
    plt.ylabel("Nombre de cellules")
    plt.title("IPN en fonction du temps")
    plt.show()

    fig = plt.figure()
    plt.plot(temps, cells.neurons)
    plt.xlabel("Temps")
    plt.ylabel("Nombre de cellules")
    plt.title("Neurones en fonction du temps")
    plt.show()

    fig = plt.figure()
    plt.plot(temps, cells.neurons_deep_layer)
    plt.xlabel("Temps")
    plt.ylabel("Nombre de cellules")
    plt.title("DL en fonction du temps")
    plt.show()

    fig = plt.figure()
    plt.plot(temps, cells.neurons_upper_layer)
    plt.xlabel("Temps")
    plt.ylabel("Nombre de cellules")
    plt.title("UL en fonction du temps")
    plt.show()

    if by_phase == True:
        fig = plt.figure()
        plt.plot(temps, cells.IPP_G1)
        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Nombre de IPP_G1 en fonction du temps")
        plt.legend()
        plt.show()

        fig = plt.figure()
        plt.plot(temps, cells.IPP_S)
        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Nombre de IPP_S en fonction du temps")
        plt.show()

        fig = plt.figure()
        plt.plot(temps, cells.IPP_G2)
        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Nombre de IPP_G2 en fonction du temps")
        plt.show()

        fig = plt.figure()
        plt.plot(temps, cells.IPP_M)
        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Nombre de IPP_M en fonctions du temps")
        plt.show()

        fig = plt.figure()
        plt.plot(temps, cells.IPN_G1)
        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Nombre de IPN_G1 en fonctions du temps")
        plt.show()

        fig = plt.figure()
        plt.plot(temps, cells.IPN_S)
        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Nombre de IPN_S en fonctions du temps")
        plt.show()

        fig = plt.figure()
        plt.plot(temps, cells.IPN_G2)
        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Nombre de IPN_G2 en fonctions du temps")
        plt.show()

        fig = plt.figure()
        plt.plot(temps, cells.IPN_M)
        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Nombre de IPN_M en fonctions du temps")
        plt.show()


def plot_several_simuls(
    temps: "np.array[float]",
    all_cells: "np.array[Cells]",
    cells_means: Cells = None,
    by_phase: bool = False,
):
    """
    Cette fonction permet de tracer le nombre de IPP en fonction du temps,
    le nombre de IPN en fonction du temps ainsi que le nombre de neurones (DL et 
    UL aussi) en fonction du temps à l'aide d'un objet de type Cells comportant 
    ces informations pour plusieurs simulations. Nous pouvons également tracer la 
    moyenne de ces simulations avec cette fonction.

    Parameters
    ------------
    temps : np.array[float]
        Tableau comportant la liste des temps dont on veut connaître le nombre de 
        chaque type de cellule.

    all_cells : np.array[Cells]
        Tableau contenant tous les objets de type cells dont nous allons nous servir
        afin de tracer les différentes courbes.

    cells_means : Cells
        Objet de type Cells contenant la moyenne des all_cells. Si ce paramètre est sur None, 
        la moyenne ne sera pas affichée.

    by_phase: bool 
        Si ce paramètre est sur vrai, nous allons également afficher les différentes
        phases.
    """

    n_simuls = len(all_cells)
    fig = plt.figure()
    for k in range(0, n_simuls):
        plt.plot(temps, all_cells[k].IPP)

    if cells_means != None:
        plt.plot(temps, cells_means.IPP, label="moyenne", color="black", linewidth=1.5)

    plt.xlabel("Temps")
    plt.ylabel("Nombre de cellules")
    plt.title("Nombre d'IPPs en fonctions du temps pour plusieurs simulations")
    plt.legend()
    plt.show()

    fig = plt.figure()
    for k in range(0, n_simuls):
        plt.plot(temps, all_cells[k].IPN)

    if cells_means != None:
        plt.plot(temps, cells_means.IPN, label="moyenne", color="black", linewidth=1.5)

    plt.xlabel("Temps")
    plt.ylabel("Nombre de cellules")
    plt.title("Nombre d'IPNs en fonctions du temps pour plusieurs simulations")
    plt.legend()
    plt.show()

    fig = plt.figure()
    for k in range(0, n_simuls):
        plt.plot(temps, all_cells[k].neurons)

    if cells_means != None:
        plt.plot(
            temps, cells_means.neurons, label="moyenne", color="black", linewidth=1.5
        )

    plt.xlabel("Temps")
    plt.ylabel("Nombre de cellules")
    plt.title("Nombre de neurones en fonctions du temps pour plusieurs simulations")
    plt.legend()
    plt.show()

    fig = plt.figure()
    for k in range(0, n_simuls):
        plt.plot(temps, all_cells[k].neurons_deep_layer)

    if cells_means != None:
        plt.plot(
            temps,
            cells_means.neurons_deep_layer,
            label="moyenne",
            color="black",
            linewidth=1.5,
        )
    plt.xlabel("Temps")
    plt.ylabel("Nombre de cellules")
    plt.title("Nombre de DL en fonctions du temps pour plusieurs simulations")
    plt.legend()
    plt.show()

    fig = plt.figure()
    for k in range(0, n_simuls):
        plt.plot(temps, all_cells[k].neurons_upper_layer)

    if cells_means != None:
        plt.plot(
            temps,
            cells_means.neurons_upper_layer,
            label="moyenne",
            color="black",
            linewidth=1.5,
        )
    plt.xlabel("Temps")
    plt.ylabel("Nombre de cellules")
    plt.title("Nombre de UL en fonctions du temps pour plusieurs simulations")
    plt.legend()
    plt.show()

    if by_phase == True:

        fig = plt.figure()

        for k in range(0, n_simuls):
            plt.plot(temps, all_cells[k].IPP_G1)

        if cells_means != None:
            plt.plot(
                temps,
                cells_means.IPP_G1,
                label="moyenne",
                color="black",
                linewidth=1.5,
            )
        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Nombre de IPP_G1 en fonctions du temps pour plusieurs simulations")
        plt.legend()
        plt.show()

        fig = plt.figure()

        for k in range(0, n_simuls):
            plt.plot(temps, all_cells[k].IPP_S)

        if cells_means != None:
            plt.plot(
                temps, cells_means.IPP_S, label="moyenne", color="black", linewidth=1.5,
            )
        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Nombre de IPP_S en fonctions du temps pour plusieurs simulations")
        plt.legend()
        plt.show()

        fig = plt.figure()

        for k in range(0, n_simuls):
            plt.plot(temps, all_cells[k].IPP_G2)

        if cells_means != None:
            plt.plot(
                temps,
                cells_means.IPP_G2,
                label="moyenne",
                color="black",
                linewidth=1.5,
            )
        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Nombre de IPP_G2 en fonctions du temps pour plusieurs simulations")
        plt.legend()
        plt.show()

        fig = plt.figure()

        for k in range(0, n_simuls):
            plt.plot(temps, all_cells[k].IPP_M)

        if cells_means != None:
            plt.plot(
                temps, cells_means.IPP_M, label="moyenne", color="black", linewidth=1.5,
            )
        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Nombre de IPP_M en fonctions du temps pour plusieurs simulations")
        plt.legend()
        plt.show()

        fig = plt.figure()

        for k in range(0, n_simuls):
            plt.plot(temps, all_cells[k].IPN_G1)

        if cells_means != None:
            plt.plot(
                temps,
                cells_means.IPN_G1,
                label="moyenne",
                color="black",
                linewidth=1.5,
            )
        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Nombre de IPN_G1 en fonctions du temps pour plusieurs simulations")
        plt.legend()
        plt.show()

        fig = plt.figure()

        for k in range(0, n_simuls):
            plt.plot(temps, all_cells[k].IPN_S)

        if cells_means != None:
            plt.plot(
                temps, cells_means.IPN_S, label="moyenne", color="black", linewidth=1.5,
            )
        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Nombre de IPN_S en fonctions du temps pour plusieurs simulations")
        plt.legend()
        plt.show()

        fig = plt.figure()

        for k in range(0, n_simuls):
            plt.plot(temps, all_cells[k].IPN_G2)

        if cells_means != None:
            plt.plot(
                temps,
                cells_means.IPN_G2,
                label="moyenne",
                color="black",
                linewidth=1.5,
            )
        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Nombre de IPN_G2 en fonctions du temps pour plusieurs simulations")
        plt.legend()
        plt.show()

        fig = plt.figure()

        for k in range(0, n_simuls):
            plt.plot(temps, all_cells[k].IPN_M)

        if cells_means != None:
            plt.plot(
                temps, cells_means.IPN_M, label="moyenne", color="black", linewidth=1.5,
            )
        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Nombre de IPN_M en fonctions du temps pour plusieurs simulations")
        plt.legend()
        plt.show()


def compare_stochastic_deterministic(
    n_simuls: int = 100, compare_mean: bool = False, type_sample: str = "Normal"
):
    """
    Cette fonction affiche le nombre de cellules en fonction du temps calculés 
    avec le modèle déterministe et le modèle stochastique. Cela permet notamment 
    de comparer les deux modèles.

    Parameters
    ------------
    n_simuls : int
        Nombre de simulations du modèle stochastique que l'on veut comparer avec le modèle déterministe.

    compare_mean: bool 
        Si ce paramètre est mis sur vrai, la comparaison va être entre la courbe 
        du modèle déterministe et la moyenne du obtenue pour le modèle stochastique.
        Dans le cas contraire, nous allons juste afficher le modèle déterministe 
        avec plusieurs courbes.

    type_sample : str 
        Permet de choisir si nous allons plutôt prendre les paramètres pour la souris
        mutant ou pour la souris normale.
    """
    temps, moy_cells, all_cells = mean_cells(
        type_sample=type_sample, with_all_cells=True, n_simuls=n_simuls
    )

    deterministic_neuro = Deterministic_simulator(type_sample)
    deterministic_temps, deterministic_cells = deterministic_neuro.to_cells()

    if compare_mean:
        fig = plt.figure()
        plt.plot(temps, moy_cells.IPP, label="Moyenne modèle stochastique")
        plt.plot(
            deterministic_temps, deterministic_cells.IPP, label="Modèle déterministe",
        )

        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Comparaison du modèle déterministe et du modèle stochastique, IPPs")
        plt.legend()
        plt.show()

        fig = plt.figure()
        plt.plot(temps, moy_cells.IPN, label="Moyenne modèle stochastique")
        plt.plot(
            deterministic_temps, deterministic_cells.IPN, label="Modèle déterministe",
        )

        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Comparaison du modèle déterministe et du modèle stochastique, IPNs")
        plt.legend()
        plt.show()

        fig = plt.figure()
        plt.plot(temps, moy_cells.neurons, label="Moyenne modèle stochastique")
        plt.plot(
            deterministic_temps,
            deterministic_cells.neurons,
            label="Modèle déterministe",
        )

        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title(
            "Comparaison du modèle déterministe et du modèle stochastique, neurones"
        )
        plt.legend()
        plt.show()

    else:
        fig = plt.figure()
        for k in range(0, n_simuls):
            plt.plot(temps, all_cells[k].IPP)
        plt.plot(
            deterministic_temps,
            deterministic_cells.IPP,
            label="Modèle déterministe",
            color="black",
            linewidth=2,
        )

        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Comparaison du modèle déterministe et du modèle stochastique, IPPs")
        plt.legend()
        plt.show()

        fig = plt.figure()
        for k in range(0, n_simuls):
            plt.plot(temps, all_cells[k].IPN)
        plt.plot(
            deterministic_temps,
            deterministic_cells.IPN,
            label="Modèle déterministe",
            color="black",
            linewidth=2,
        )

        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title("Comparaison du modèle déterministe et du modèle stochastique, IPNs")
        plt.legend()
        plt.show()

        fig = plt.figure()
        for k in range(0, n_simuls):
            plt.plot(temps, all_cells[k].neurons)
        plt.plot(
            deterministic_temps,
            deterministic_cells.neurons,
            label="Modèle déterministe",
            color="black",
            linewidth=2,
        )

        plt.xlabel("Temps")
        plt.ylabel("Nombre de cellules")
        plt.title(
            "Comparaison du modèle déterministe et du modèle stochastique, neurones"
        )
        plt.legend()
        plt.show()

