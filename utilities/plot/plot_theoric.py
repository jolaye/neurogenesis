# Fonction utiles afin de vérifier si les donnéees expérimentales/ simulations
# sont en accord avec la théorie.

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from utilities.functions.moments import mean_cells
from theoric.cells import *
import warnings

warnings.filterwarnings("ignore")


def compare_theo_exp(
    type_cell: str,
    type_sample: str = "Normal",
    type_division: str = "constant",
    data_file: str = "data/experimental/data_exp.csv",
):
    """
    Cette fonction permet de comparer les données expérimentales, avec la valeur 
    théorique de l'espérance et de la variance. Pour un type de cellule donnée, elle
    trace les données expérimentales en fonction du temps, ainsi que l'espérance du
    nombre de cellules au cours du temps dans une fenêtre de 2 fois l'écart-type.  

    Parameters
    ------------
    type_cell : str
        Type de la cellule dont on souhaite obtenir la comparaison théorique/expérimentale.

    type_sample : str
        Permet de choisir si on va effectuer la comparaison pour le jeu de données
        correspondant aux souris normales ou aux souris mutantes.
    
    type_division : str
        Permet de choisir si on va effectuer la comparaison pour une loi de temps de division
        constante, gamma, exponentielle....
    
    data_file : str
        Nom du fichier csv qui possède les données expérimentales dedans.
    """
    # Récupération des données expérimentales
    data = pd.read_csv(data_file)
    data = data[data["Samples"] == type_sample]
    times_data = data["Times"].values
    n_cells = data[type_cell].values

    if (type_cell == "N") or (type_cell == "UL") or (type_cell == "DL"):
        min_time_neurons = 12.5
        max_time_neurons = (
            16.5  # Les données sont utilisables uniquement dans l'intervalle 13.5 16.5
        )

        indexes_good_values = np.where(
            (times_data >= min_time_neurons) & (times_data < max_time_neurons)
        )
        times_data = times_data[indexes_good_values]
        n_cells = n_cells[indexes_good_values]

    # Récupération des données théoriques
    n_point = 1000
    coeff_end = 1.2
    index_start = 0
    index_end = -1
    start_times = times_data[index_start]
    end_times = times_data[index_end] * coeff_end

    times_theo = np.linspace(start_times, end_times, n_point)
    esperances_theo = np.zeros(len(times_theo))
    ecart_type_theo = np.zeros(len(times_theo))

    if type_cell == "IPP":
        type_cell_class = IPPs(type_sample, type_division)
    if type_cell == "IPN":
        type_cell_class = IPNs(type_sample, type_division)
    if type_cell == "N":
        type_cell_class = Neurons(type_sample, type_division)
    if type_cell == "DL":
        type_cell_class = DeepLayers(type_sample, type_division)
    if type_cell == "UL":
        type_cell_class = UpLayers(type_sample, type_division)

    for k in range(0, len(times_theo)):

        esperances_theo[k] = type_cell_class.esperance(times_theo[k])
        ecart_type_theo[k] = type_cell_class.ecart_type(times_theo[k])

    coeff_ecart_type = 2
    fig = plt.figure()
    plt.plot(
        times_theo,
        esperances_theo,
        label="{} théorique".format(type_cell),
        color="blue",
    )
    plt.plot(
        times_theo,
        esperances_theo + coeff_ecart_type * ecart_type_theo,
        color="blue",
        linestyle="dashed",
    )
    plt.plot(
        times_theo,
        esperances_theo - coeff_ecart_type * ecart_type_theo,
        color="blue",
        linestyle="dashed",
    )

    plt.plot(
        times_data,
        n_cells,
        label="{} expérimental".format(type_cell),
        marker="x",
        linestyle=" ",
        color="black",
    )
    plt.legend()
    plt.xlabel("Temps")
    plt.ylabel("Nombre de cellules")
    plt.title(
        "Comparaison valeurs théoriques et valeurs expérimentales pour {}".format(
            type_cell
        )
    )
    plt.show()


def compare_theo_simul(
    type_cell: str,
    type_moment: str = "Esperance",
    type_sample: str = "Normal",
    type_division: str = "constant",
):
    """
    Cette fonction permet de comparer la valeur théorique des moments avec la valeur
    estimée par simulations. Cela sert notamment à vérifier les valeurs que nous 
    avons trouvée pour les moments.

    Parameters
    ------------
    type_cell : str
        Nom du type de cellule dont on veut regarder si la valeur théorique de 
        l'un de ses moments est en accord avec les simulations.

    type_moment : str
        Nom du moment que dont on veut effectuer la comparaison.
    
    type_sample : str
        Permet de savoir si nous allons charger les paramètres liés au jeu de souris
        normales ou mutantes afin d'effectuer cette comparaison.

    type_division : str
        Nom de la loi que nous allons utiliser pour les durées des cycles cellulaires.
    """
    ## Simulations
    if type_moment == "Esperance":
        moments_times, moments_cells = mean_cells(
            type_division, type_sample=type_sample
        )
    if type_moment == "Variance":
        moments_times, moments_cells = var_cells(type_division, type_sample=type_sample)

    ## Récupération des différentes données
    start_time = 0
    end_time = 30
    n_point = 100
    times_theo = np.linspace(start_time, end_time, n_point)
    moments_theo = np.zeros(len(times_theo))

    if type_cell == "IPP":
        type_cell_class = IPPs(type_sample, type_division)
        moments_to_plot = moments_cells.IPP

    if type_cell == "IPN":
        type_cell_class = IPNs(type_sample, type_division)
        moments_to_plot = moments_cells.IPN

    if type_cell == "N":
        type_cell_class = Neurons(type_sample, type_division)
        moments_to_plot = moments_cells.neurons

    if type_cell == "DL":
        type_cell_class = DeepLayers(type_sample, type_division)
        moments_to_plot = moments_cells.neurons_deep_layers

    if type_cell == "UL":
        type_cell_class = UpLayers(type_sample, type_division)
        moments_to_plot = moments_cells.neurons_upper_layers

    for k in range(0, len(times_theo)):
        if type_moment == "Esperance":
            moments_theo[k] = type_cell_class.esperance(times_theo[k])
        if type_moment == "Variance":
            moments_theo[k] = type_cell_class.variance(times_theo[k])

    fig = plt.figure()
    plt.plot(
        times_theo,
        moments_theo,
        label=type_moment + " {} théorique".format(type_cell),
        color="blue",
    )

    plt.plot(
        times_data,
        n_cells,
        label=type_moment + " {} simulations".format(type_cell),
        color="black",
    )
    plt.legend()
    plt.xlabel("Temps")
    plt.ylabel("Nombre de cellules")
    plt.title(
        "Comparaison valeurs théoriques/simulées pour "
        + type_moment
        + " {}".format(type_cell)
    )
    plt.show()

