import json
from cemone import *
from cells import Cells


class Deterministic_simulator(object):
    """
    Cette classe permet de simuler la neurogenèse à l'aide du modèle Cémone 
    de l'article principal. 

    Examples
    --------
        Neuro = Neurogenese() \n
        temps, cells = Neuro.to_cells()

    Attributes
    ------------
    type_sample : str 
        Permet de choisir si nous allons plutôt prendre les paramètres pour la souris
        mutant ou pour la souris normale.
    """

    def __init__(self, type_sample: str):
        if type_sample == "Normal":
            parameters_file = "data/simulation/normal_parameters.json"
        if type_sample == "Mutant":
            parameters_file = "data/simulation/mutant_parameters.json"
        self.parameters = json.load(open(parameters_file, "r"))
        var_arg = [
            self.parameters["AP_enter_neuro"]["scaling_factor"] / 24,
            self.parameters["AP_enter_neuro"]["location_inflexion_rise"],
            self.parameters["AP_enter_neuro"]["slope_inflexion_rise"],
            self.parameters["AP_enter_neuro"]["location_inflexion_decay"],
            self.parameters["AP_enter_neuro"]["slope_inflexion_decay"],
            self.parameters["IPP_or_IPN"]["moins_inf_function_rate"],
            self.parameters["IPP_or_IPN"]["slope_function_rate"],
            self.parameters["IPP_or_IPN"]["location_function_rate"],
            self.parameters["IPP_or_IPN"]["plus_inf_function_rate"],
            self.parameters["AP_to_IP"]["moins_inf_function_rate"],
            self.parameters["AP_to_IP"]["slope_function_rate"],
            self.parameters["AP_to_IP"]["location_function_rate"],
            self.parameters["AP_to_IP"]["plus_inf_function_rate"],
            self.parameters["simulation_parameters"]["time_start"],
            self.parameters["simulation_parameters"]["time_end"],
        ]

        self.times, self.IPPs, self.IPNs, self.Neurons, _ = compute_sol(var_arg)

    def to_cells(self) -> ("np.array[float]", Cells):
        """
        Cette fonction permet d'avoir les résultats de la simulation de la 
        neurogenèse avec le modèle déterministe avec le type Cells.

        Returns
        ------------
        self.times : "np.array[float]"
            Tableau qui contient les temps où on a calculé le nombre de neurones
            pour chaque type de cellule.
        
        deterministic_cells : Cells
            Contient les informations des résultats de la neurogenèse avec le 
            modèle déterministe avec le type Cells.
        """
        deterministic_cells = Cells(len(self.times))
        deterministic_cells.IPP = self.IPPs
        deterministic_cells.IPN = self.IPNs
        deterministic_cells.IP = self.IPPs + self.IPNs
        deterministic_cells.neurons = (
            self.Neurons
        )  # Pas de couches dans le modèle déterministe

        return self.times, deterministic_cells
