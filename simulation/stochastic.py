import numpy as np
import json
from simulation import *
from utilities.utils import sigmoide, compute_index_time

from simulation.type_event.AP_to_IPP import AP_to_IPP
from simulation.type_event.AP_to_IPN import AP_to_IPN
from simulation.type_event.AP_to_neurons import AP_to_neurons


class Stochastic_simulator(object):
    """
    Cette classe permet de simuler la neurogenèse en stochastique. Elle contient notamment la fonction
    qui permet de simuler cette neurogenèse par la variante Extrande de Gillepsie, ainsi
    que la fonction permettant de calculer les taux auxquels les progéniteurs apicaux
    entrent dans dans la neurogenèse.

    Examples
    --------
    Neuro = Neurogenese() \n
    temps, cells = Neuro.simulate()

    Attributes
    ------------
    type_division : str
        Si ce paramètre est à "constant", la neurogenèse se fera avec des temps 
        de cycle constant. Si il est à "gamma", les temps de cycles (ou phases) seront
        aléatoires et suivront une loi gamma dont les paramètres sont préalablement
        remplis dans le fichier parameters.json.

    type_phase : str
        Décide quelle loi va être choisie pour la répartition des phases du cycle
        cellulaire.

    type_sample : str 
        Permet de choisir si nous allons plutôt prendre les paramètres pour la souris
        mutant ou pour la souris normale.
    """

    def __init__(
        self,
        type_division: "str" = "constant",
        type_phase: "str" = "constant",
        type_sample: str = "Normal",
    ):
        self.initialize_simu_parameters(type_sample, type_division, type_phase)
        # Utilisation des paramètres pour d'autres classes
        self.AP_to_IPP = AP_to_IPP(self.parameters)
        self.AP_to_IPN = AP_to_IPN(self.parameters)
        self.AP_to_neurons = AP_to_neurons(self.parameters)

    def simulate(self) -> "(np.array[float], Cells)":
        """
        Ce programme simule la neurogenèse en utilisant la variante Extrande de 
        l'algorithme de Gillespie. Cette variante permet d'utiliser des propensions 
        qui varient avec le temps. Elle renvoit un tableau correspondant aux temps, 
        et un ficher Cells contenant pour ces temps le nombre de cellules pour chaque
        types (IPP, neurones ...).

        Returns
        ------------
        times : np.array[float]
            Tableau contenant les temps où nous avons calculé le nombre de 
            cellules pour chaque type.
        
        cells : Cells 
            Contient pour chaque type de cellules le nombre de cellules, et ce
            pour chaque temps du tableau times.
        """

        ## Récupération paramètres simulations
        time_start = self.parameters["simulation_parameters"]["time_start"]
        time_end = self.parameters["simulation_parameters"]["time_end"]
        step_time = self.parameters["simulation_parameters"]["step_time"]
        propensity_bound = self.parameters["simulation_parameters"]["propensity_bound"]

        ## Simulations
        times = np.arange(time_start, time_end, step_time)
        cells = Cells(
            len(times)
        )  # Permet d'avoir l'évolution de chaque type cellulaire au cours du temps

        time_since_beginning = time_start  # Temps depuis le début de la simulation (on va le mettre à jour à chaque itération). On le commence à "time_start".

        while time_since_beginning < time_end:

            time_before_event = (
                -np.log(np.random.uniform()) / propensity_bound
            )  # Simulation de la loi exponentielle de paramètre la borne de propensité

            if (
                time_since_beginning + time_before_event > time_end
            ):  # Si le temps avant la prochaine division est après la fin de la simulation, on finit la simulation sans mettre à jour

                time_since_beginning = time_end

            else:

                time_since_beginning += time_before_event
                propensities = self.compute_propensities(
                    time_since_beginning
                )  #  Calcul les propensités au temps donné

                which_division_occurs = (
                    np.random.uniform()
                )  # Variable aléatoire qui donne quelle division va avoir lieu

                if (
                    np.sum(propensities) >= which_division_occurs * propensity_bound
                ):  # Si cette condition n'est pas vérifiée,
                    # il n'y a pas de division (c'est la réaction "en plus" qui a lieu).

                    index_type_division = np.where(
                        np.cumsum(propensities)
                        - propensity_bound * which_division_occurs
                        > 0
                    )[0][
                        0
                    ]  # Premier indice > 0

                    if (
                        self.parameters["div_name"][index_type_division]
                        == "AP_to_neurons"
                    ):

                        cells = self.AP_to_neurons.update_cells(
                            cells, times, time_since_beginning
                        )

                    if self.parameters["div_name"][index_type_division] == "AP_to_IPN":

                        cells = self.AP_to_IPN.update_cells(
                            cells, times, time_since_beginning
                        )

                    if self.parameters["div_name"][index_type_division] == "AP_to_IPP":

                        cells = self.AP_to_IPP.update_cells(
                            cells, times, time_since_beginning
                        )

            index = compute_index_time(time_since_beginning, times)

        # Calculs du nombre de cellules pour des types plus généraux à l'aide des précédents calculs
        cells.neurons = cells.neurons_deep_layer + cells.neurons_upper_layer
        cells.IPP = cells.IPP_G1 + cells.IPP_S + cells.IPP_G2 + cells.IPP_M
        cells.IPN = cells.IPN_G1 + cells.IPN_S + cells.IPN_G2 + cells.IPN_M
        cells.IP = cells.IPP + cells.IPN
        cells.ratio_injection = cells.counts_second_injection / (
            2 * cells.counts_first_injection
        )

        return times, cells

    def compute_propensities(self, time: float) -> "np.array[float]":
        """
        Cette fonction permet de calculer les propensités pour chaque type de divisions.
        Ces propensités varient en fonction du temps et sont calculées à l'aide 
        des fonctions liées à chaque type de division, ainsi qu'au taux d'entrée 
        dans la neurogenèse. 

        Parameters
        ------------
        time : float 
            Temps pour lequel nous voulons connaître les propensités.
        
        Returns
        ------------
        propensities : np.array[float]
            Tableau contenant pour chaque type de division la propensité. L'ordre
            dans lequel est donnée la propensité est exactement le même ordre que 
            l'ordre mis pour les types de division dans le fichier json correspondant
            aux paramètres.
        """
        propensities = np.zeros(len(self.parameters["div_name"]))

        # Pour chaque type de division calcule la propensité.
        for k in range(0, len(self.parameters["div_name"])):

            if self.parameters["div_name"][k] == "AP_to_neurons":

                propensities[k] = self.AP_to_neurons.rate_function(time) * self.rate_AP(
                    time
                )

            if self.parameters["div_name"][k] == "AP_to_IPN":

                propensities[k] = self.AP_to_IPN.rate_function(time) * self.rate_AP(
                    time
                )

            if self.parameters["div_name"][k] == "AP_to_IPP":

                propensities[k] = self.AP_to_IPP.rate_function(time) * self.rate_AP(
                    time
                )

        return propensities

    def rate_AP(self, time: float) -> float:
        """
        Cette fonction permet de calculer le taux auquel les progéniteurs apicaux
        rentrent dans la neurogenèse pour un temps donné. Ce taux est calculé avec 
        une multiplication de deux fonctions sigmoides permettant d'abord une phase 
        ascendante et ensuite une phase descendante.

        Parameters
        ------------
        time : float 
            Temps pour lequel nous voulons connaître le taux auquel les progéniteurs
            apicaux rentrent dans la neurogenèse.
        """
        return (
            self.parameters["AP_enter_neuro"]["scaling_factor"]
            * (
                1
                - sigmoide(
                    time,
                    self.parameters["AP_enter_neuro"]["slope_inflexion_rise"],
                    self.parameters["AP_enter_neuro"]["location_inflexion_rise"],
                )
            )
            * sigmoide(
                time,
                self.parameters["AP_enter_neuro"]["slope_inflexion_decay"],
                self.parameters["AP_enter_neuro"]["location_inflexion_decay"],
            )
        )

    def initialize_simu_parameters(
        self, type_sample: str, type_division: str, type_phase: str
    ):
        """ 
        Cette fonction permet d'initialiser le fichier json contenant les différents
        paramètres liés à notre optimisation. Dans un premier temps, on charge
        soit les paramètres du jeu de souris normale, soit les paramètres du jeu 
        de souris mutante puis on rajoute à ceux-ci le type de loi que l'on va 
        utiliser pour les durées de cycles cellulaires et les proportions des phases.

        Parameters
        ------------
        type_sample : str 
            Permet de choisir si nous allons plutôt prendre les paramètres pour la souris
            mutant ou pour la souris normale.

        type_division : str
            Si ce paramètre est à "constant", la neurogenèse se fera avec des temps 
            de cycle constant. Si il est à "gamma", les temps de cycles (ou phases) seront
            aléatoires et suivront une loi gamma dont les paramètres sont préalablement
            remplis dans le fichier parameters.json.

        type_phase : str
            Décide quelle loi va être choisie pour la répartition des phases du cycle
            cellulaire.
        """

        if type_sample == "Normal":
            simulation_parameters_file = "data/simulation/normal_parameters.json"
        if type_sample == "Mutant":
            simulation_parameters_file = "data/simulation/mutant_parameters.json"

        self.parameters = json.load(open(simulation_parameters_file, "r"))
        self.parameters["simulation_parameters"]["type_division"] = type_division
        self.parameters["simulation_parameters"]["type_phase"] = type_phase
