import numpy as np
import json
from simulation import Cells
from utilities.utils import sigmoide, compute_index_time, gaussian, gamma


class AP_to_neurons(object):
    """
    Cette classe contient toutes les fonctions liées à la division : AP -> AP + N.
    Ces fonctions sont celles qui permettent de calculer le taux auxquel on a ce type 
    de division, ainsi que celle qui décrit comment se met à jour le nombre de 
    cellules après que ce type de division ait lieu. On fera appel à cette classe
    durant la simulation de la neurogenèse.

    Attributes
    ------------
    parameters: json
        json contenant toutes les infomations concernant la neurogenèse, dont 
        celles liées à cette division.
    """

    def __init__(self, parameters: json):

        self.parameters = parameters

    def rate_function(self, time: float) -> float:
        """
        Cette fonction permet de calculer le taux auquel arrive la division 
        AP -> AP + N. On utilise pour la calculer une fonction sigmoide (comme 
        pour les autres fonctions de ce type). 

        Parameters
        ------------
        time: float
            Temps auquel on veut calculer le taux où on a la division AP -> AP + N.
        """
        return 1 - sigmoide(
            time,
            self.parameters["AP_to_IP"]["slope_function_rate"],
            self.parameters["AP_to_IP"]["location_function_rate"],
            self.parameters["AP_to_IP"]["plus_inf_function_rate"],
            self.parameters["AP_to_IP"]["moins_inf_function_rate"],
        )

    def test_deep_layer(self, time: float) -> int:
        """
        Cette fonction permet de savoir si le neurone créé va appartenir à la 
        couche profonde ou non. Pour cela, on calcule la probabilité d'appartenir 
        à la couche profonde (à l'aide de g'une fonction sigmoide) puis on tire 
        une variable aléatoire uniforme qui doit être inférieur à cette probabilité 
        si on veut qu'elle appartienne à la couche profonde (dans le cas contraire, 
        le neurone appartiendra à la couche du haut). 
        
        Parameters
        ------------
        time : float 
            Temps dont on veut obtenir le fait que le neurone créé va appartenir 
            à la couche profonde.
        """

        index_deep_layer = 0
        index_up_layer = 1

        score_deep_layer = sigmoide(
            time,
            self.parameters["deep_or_up"]["slope_function_rate"],
            self.parameters["deep_or_up"]["location_function_rate"],
            self.parameters["deep_or_up"]["plus_inf_function_rate"],
            self.parameters["deep_or_up"]["moins_inf_function_rate"],
        )  # Calcul la proba d'appartenir à l'une des couches.

        return np.random.uniform() < score_deep_layer

    def update_cells(
        self, cells: Cells, times: "np.array[float]", time_since_beginning: float,
    ) -> Cells:
        """
        Cette fonction permet de mettre à jour le nombre de cellules après que 
        la division AP -> AP + N (on augmente de 1 le nombre de neurones) ait eu lieu.

        Cette fonction consiste d'abord en le fait de devoir calculer les indices 
        auxquels ont lieu les changements à l'aide de la fonction compute_index_time, 
        puis de mettre à jour pour les valeurs voulues.

        Parameters
        ------------
        cells : Cells
            Objet de type Cells que l'on veut mettre à jour pour faire apparaître 
            l'effet de la division qui a eu lieu.

        times : "np.array[float]"   
            Tableau contenant tous les temps liés au fichier Cells.
        
        time_since_beginning : float
            Temps qui s'est passé depuis le début de la simulation de la neurogenèse.
        

        Returns
        ------------
        cells : Cells 
            Retourne un objet de type Cells contenant toutes les modifications liées à 
            toutes les divisions qui ont eu lieu.
        """

        time_div_to_neuron = time_since_beginning
        index_time_event = compute_index_time(time_div_neuron, times)

        # Rajout éléments pour courbes
        if self.test_deep_layer(time_div_to_neuron):
            cells.neurons_deep_layer[index_time_event:] += 1
        else:
            cells.neurons_upper_layer[index_time_event:] += 1

        return cells
