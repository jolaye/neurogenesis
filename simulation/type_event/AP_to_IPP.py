import numpy as np
import json
from simulation import Cells
from utilities.utils import *


class AP_to_IPP(object):
    """
    Cette classe contient toutes les fonctions liées à la division : AP -> AP + IPP.
    Ces fonctions sont celles qui permettent de calculer le taux auxquel on a ce type 
    de division, ainsi que celle qui décrit comment se met à jour le nombre de 
    cellules après que ce type de division ait lieu. On fera appel à cette classe
    durant la simulation de la neurogenèse.

    Attributes
    ------------
    parameters: json
        json contenant toutes les infomations concernant la neurogenèse, dont 
        celles liées à cette division.
    """

    def __init__(self, parameters: json):

        self.parameters = parameters

    def rate_function(self, time: float) -> float:
        """
        Cette fonction permet de calculer le taux auquel arrive la division 
        AP -> AP + IPP. On utilise pour la calculer une fonction sigmoide (comme 
        pour les autres fonctions de ce type). 

        Parameters
        ------------
        time: float
            Temps auquel on veut calculer le taux où on a la division AP -> AP + IPP.
        """
        return sigmoide(
            time,
            self.parameters["AP_to_IP"]["slope_function_rate"],
            self.parameters["AP_to_IP"]["location_function_rate"],
            self.parameters["AP_to_IP"]["plus_inf_function_rate"],
            self.parameters["AP_to_IP"]["moins_inf_function_rate"],
        ) * (
            sigmoide(
                time,
                self.parameters["IPP_or_IPN"]["slope_function_rate"],
                self.parameters["IPP_or_IPN"]["location_function_rate"],
                self.parameters["IPP_or_IPN"]["plus_inf_function_rate"],
                self.parameters["IPP_or_IPN"]["moins_inf_function_rate"],
            )
        )

    def time_phase_cycle(self, type_cell: str) -> float:
        """
        Cette fonction retourne pour une cellule la durée de son cycle c'est-à-dire
        le temps avant qu'elle ne se divise. Ce temps peut être soit constant 
        (dans le cadre du premier modèle simple), soit aléatoire (loi gamma, beta....).

        Parameters
        ------------
        type_cell : str
            Le type de la cellule dont on cherche la durée du cycle.
        """

        if self.parameters["simulation_parameters"]["type_division"] == "constant":

            duration_cycle = self.parameters["age_division"][type_cell]
            times_phase_cycle = self.compute_cumulate_phase(type_cell) * duration_cycle

        if self.parameters["simulation_parameters"]["type_division"] == "gamma":

            duration_cycle = gamma(
                self.parameters["age_division"][type_cell],
                self.parameters["var_age_division"][type_cell],
            )
            times_phase_cycle = self.compute_cumulate_phase(type_cell) * duration_cycle

        if self.parameters["simulation_parameters"]["type_division"] == "exponential":

            duration_cycle = exponential(
                1 / self.parameters["age_division"][type_cell],
            )
            times_phase_cycle = self.compute_cumulate_phase(type_cell) * duration_cycle

        if self.parameters["simulation_parameters"]["type_division"] == "symetric_beta":

            duration_cycle = symetric_beta(
                self.parameters["age_division"][type_cell],
                self.parameters["var_age_division"][type_cell],
            )
            times_phase_cycle = self.compute_cumulate_phase(type_cell) * duration_cycle

        return times_phase_cycle

    def compute_cumulate_phase(self, type_cell: str,) -> "np.array[float]":
        """
        Cette fonction permet de calculer quel pourcentage de la durée totale de 
        phase va durer chacune des phases du cycle cellulaire, puis retourne
        les proportions cumulées. Le tirage est soit constant, soit beta soit 
        multinomial.

        Parameters
        ------------
        type_cell : str
            Type de la cellule qui va se diviser dont on veut connaître le pourcentage
            de durée de chaque phase.
        """

        if self.parameters["simulation_parameters"]["type_phase"] == "constant":
            duration_phases = np.array(self.parameters["percent_phase"][type_cell])

        if self.parameters["simulation_parameters"]["type_phase"] == "beta":
            duration_phases = beta_phase(
                self.parameters["percent_phase"][type_cell],
                self.parameters["var_age_division"][type_cell + "_S"],
            )

        if self.parameters["simulation_parameters"]["type_phase"] == "multinomial":
            duration_phases = multinomial_phase(
                self.parameters["percent_phase"][type_cell],
            )

        return np.cumsum(duration_phases)

    def update_by_event(
        self,
        cells_to_update: Cells,
        type_cell: str,
        debut_cycle: float,
        times: "np.array[float]",
        injection: bool = False,
    ) -> (Cells, float):
        """
        Cette fonction permet la mise à jour de l'objet "cells" lors de chaque 
        évènement. Elle tire les instants où le cycle se termine, puis ajoute des 
        cellules aux instants qui correspondent au moment où la phase se produit.

        Parameters
        ------------
        cells_to_update: Cells
            L'objet cells que l'on va mettre à jour.

        type_cell: str
            Type de cellule que l'on va mettre à jour. Important car la durée des
            cycles n'est pas la même en fonction des types.  

        debut_cycle: float
            Instant où le cycle commence.
        
        times: "np.array[float]"
            Tableau où la liste des temps associés à l'objet Cells et à la neurogenèse 
            sont.

        injection : bool
            Paramètre qui permet de savoir si la cellule créée a été marquée lors
            de la première injection ou non.

        Returns
        ------------
        cells_to_update : Cells
            Objet Cells qui aura fini d'être mis à jour une fois que la fonction
            sera finie.
        
        time_time_end_div : float
            Instant où la division se termine. On le renvoie car on en aura sûrement 
            besoin par la suite.
        """
        # On calcule les différents instants des phases du cycle
        time_div_to_G1 = debut_cycle
        (
            time_div_to_S,
            time_div_to_G2,
            time_div_to_M,
            time_end_div,
        ) = debut_cycle + self.time_phase_cycle(type_cell)

        # On calcule à quels index ils sont associés dans le tableau "times"
        # comprenant les temps associés à l'objet Cells
        index_time_G1 = compute_index_time(time_div_to_G1, times,)
        index_time_S = compute_index_time(time_div_to_S, times,)
        index_time_G2 = compute_index_time(time_div_to_G2, times,)
        index_time_M = compute_index_time(time_div_to_M, times,)
        index_time_end_div = compute_index_time(time_end_div, times,)

        # Savoir si ils seront reconnus en cas d'injection
        if (
            (time_div_to_S <= self.parameters["time_first_injection"])
            & (self.parameters["time_first_injection"] < time_div_to_G2)
            & (type_cell == "IPP")
        ):  # Première injection

            cells_to_update.counts_first_injection += 1
            injection = True

        elif injection:  # Seconde injection

            cells_to_update.counts_second_injection[index_time_S:index_time_G2] += 1

        else:
            injection = False  # Pas reconnu par injection

        # On update à l'aide de ces indices en fonction du type de cellule que c'est
        if type_cell == "IPP":
            cells_to_update.IPP_G1[index_time_G1:index_time_S] += 1
            cells_to_update.IPP_S[index_time_S:index_time_G2] += 1
            cells_to_update.IPP_G2[index_time_G2:index_time_M] += 1
            cells_to_update.IPP_M[index_time_M:index_time_end_div] += 1

        if type_cell == "IPN":
            cells_to_update.IPN_G1[index_time_G1:index_time_S] += 1
            cells_to_update.IPN_S[index_time_S:index_time_G2] += 1
            cells_to_update.IPN_G2[index_time_G2:index_time_M] += 1
            cells_to_update.IPN_M[index_time_M:index_time_end_div] += 1

        return cells_to_update, time_end_div, injection

    def add_neurons(
        self, cells: Cells, time_new_neurons: float, times: "np.array[float]"
    ) -> Cells:
        """
        Mise à jour de la génération de nouveaux neurones dans le fichier Cells
        en les rajoutant à partir de l'instant où ils ont été créés, et à quelle 
        couche ils appartiennent à l'aide de la fonction test_deep_layer.

        Parameters
        ------------
        cells : Cells
            Objet de type Cells que l'on va mettre à jour.
        
        time_new_neurons : float
            Instant où ont été générés 2 nouveaux neurones.
        
        times : float
            Tableau lié à l'objet Cells.
        
        Returns
        ------------
        cells : Cells
            Objet de type Cells mis à jour à l'aide de la fonction.
        """
        index_time_div_to_neuron = compute_index_time(time_new_neurons, times,)

        #  Deux fois le même test car deux neurones sont créés
        if self.test_deep_layer(time_new_neurons):
            cells.neurons_deep_layer[index_time_div_to_neuron:] += 1
        else:
            cells.neurons_upper_layer[index_time_div_to_neuron:] += 1

        if self.test_deep_layer(time_new_neurons):
            cells.neurons_deep_layer[index_time_div_to_neuron:] += 1
        else:
            cells.neurons_upper_layer[index_time_div_to_neuron:] += 1

        return cells

    def test_deep_layer(self, time: float) -> int:
        """
        Cette fonction permet de savoir si le neurone créé va appartenir à la 
        couche profonde ou non. Pour cela, on calcule la probabilité d'appartenir 
        à la couche profonde (à l'aide de g'une fonction sigmoide) puis on tire 
        une variable aléatoire uniforme qui doit être inférieur à cette probabilité 
        si on veut qu'elle appartienne à la couche profonde (dans le cas contraire, 
        le neurone appartiendra à la couche du haut). 
        
        Parameters
        ------------
        time : float 
            Temps dont on veut obtenir le fait que le neurone créé va appartenir 
            à la couche profonde.
        """

        index_deep_layer = 0
        index_up_layer = 1

        score_deep_layer = sigmoide(
            time,
            self.parameters["deep_or_up"]["slope_function_rate"],
            self.parameters["deep_or_up"]["location_function_rate"],
            self.parameters["deep_or_up"]["plus_inf_function_rate"],
            self.parameters["deep_or_up"]["moins_inf_function_rate"],
        )  # Calcul la proba d'appartenir à l'une des couches.

        return np.random.uniform() < score_deep_layer

    def update_cells(
        self, cells: Cells, times: "np.array[float]", time_since_beginning: float,
    ) -> Cells:
        """
        Cette fonction permet de mettre à jour le nombre de cellules après que 
        la division AP -> AP + IPP (on augmente de 1 le nombre de IPP) ait eu lieu, 
        puis après que la divisions associées IPP -> 2IPN  (on diminue de 1 le nombre 
        de IPN, et on augmente de 2 le nombre de IPP) ait eu lieu, et enfin après
        que les deux division associées IPN -> 2N  (on diminue de 2 le nombre 
        de IPN, et on augmente de 4 le nombre de neurones car la division a lieu 
        2 fois) ont eu lieu.

        Cette fonction consiste d'abord en le fait de devoir calculer les indices 
        auxquels ont lieu les changements à l'aide de la fonction compute_index_time, 
        puis de mettre à jour pour les valeurs voulues.

        Parameters
        ------------
        cells : Cells
            Objet de type Cells que l'on veut mettre à jour pour faire apparaître 
            les effets des divisions qui ont eu lieu.

        times : "np.array[float]"   
            Tableau contenant tous les temps liés au fichier Cells.
        
        time_since_beginning : float
            Temps qui s'est passé depuis le début de la simulation de la neurogenèse.

        Returns
        ------------
        cells : Cells 
            Retourne un objet de type Cells contenant toutes les modifications liées à 
            toutes les divisions qui ont eu lieu.
        """
        # Update différentes phases liées à division IPP en IPN
        cells, time_new_IPNs, injection = self.update_by_event(
            cells, "IPP", time_since_beginning, times
        )

        # Différentes phases liées à division IPN en neurones (2 fois car deux IPNs ont été créés suite à division IPP)
        cells, time_new_neurons_1, _ = self.update_by_event(
            cells, "IPN", time_new_IPNs, times, injection
        )
        cells, time_new_neurons_2, _ = self.update_by_event(
            cells, "IPN", time_new_IPNs, times, injection
        )

        cells = self.add_neurons(cells, time_new_neurons_1, times)
        cells = self.add_neurons(cells, time_new_neurons_2, times)

        return cells
