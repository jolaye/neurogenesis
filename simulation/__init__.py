# Ce script permettra d'importer facilement les éléments contenus dans le dossier
# facilement par la suite. Notamment, il permet au répertoire d'être considéré
# comme un module, et de pouvoir importer les fichiers contenus dans ce répertoire
# comme des modules.

import pkgutil
import inspect

__all__ = []

for loader, name, is_pkg in pkgutil.walk_packages(__path__):

    module = loader.find_module(name).load_module(name)

    for name, value in inspect.getmembers(module):

        if name.startswith("__"):
            continue

        globals()[name] = value
        __all__.append(name)

