import numpy as np


class Cells(object):
    """
    Cette classe introduit le type de variables "Cells", qui stock:
    - le nombre de cellules pour chaque temps et pour chaque type (IPP, IPN, neurones...) 
    dans un tableau python,
    - le nombre de IPP/IPN en phase S reconnus lors des injections.

    Cela se stock sous forme de tableau que l'on mettra à jour au fur et à mesure 
    des simulations.

    Attributes
    ------------
    len_time : int
        La taille du tableau contenant le temps dans la neurogenèse.
    """

    def __init__(self, len_time: int):

        # Informations courbes
        self.IPP_G1 = np.zeros(len_time)  # On met chaque étape du cycle (G1, S, G2, M)
        self.IPP_S = np.zeros(len_time)
        self.IPP_G2 = np.zeros(len_time)
        self.IPP_M = np.zeros(len_time)
        self.IPP = np.zeros(len_time)

        self.IPN_G1 = np.zeros(len_time)  # On met chaque étape du cycle (G1, S, G2, M)
        self.IPN_S = np.zeros(len_time)
        self.IPN_G2 = np.zeros(len_time)
        self.IPN_M = np.zeros(len_time)
        self.IPN = np.zeros(len_time)

        self.neurons_deep_layer = np.zeros(len_time)
        self.neurons_upper_layer = np.zeros(len_time)

        self.counts_first_injection = 0
        self.counts_second_injection = np.zeros(len_time)

