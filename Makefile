demo: 
	@python ./scripts/plot_simuls.py -n 100 -c "gamma"

stocha_deter_comparison: 
	@python ./scripts/compare_stochastic_deterministic.py -n 100 -m 0 -s "Mutant"
		
exp_theo_comparison:
	@python ./scripts/compare_theo_exp.py
	
optimize: 
	@python ./scripts/optimize.py -f "layers" -m "genetic"
		

